import os, sys
from PIL import Image

if __name__=='__main__':
    
    path = sys.argv[1]
    
    for fn in os.listdir(path):
        _, ext = os.path.splitext(fn)
        if ext.lower() == '.jpg':
            fp = os.path.join(path,fn)
            im = Image.open(fp)
            im.load() 
            print fn 
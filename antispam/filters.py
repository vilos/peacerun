from django.forms import EmailField, URLField, Textarea
from django.utils.timezone import now
from django.utils.encoding import force_text
from mezzanine.utils.views import ip_for_request
from .models import BlacklistDef, SpamLog



def check_spam(data):
    user_ip = data.get('user_ip', '')
    email_to = data.get('email', '')
    message = force_text(data.get('message', ''))

    for bd in BlacklistDef.objects.all():
        if (user_ip and bd.user_ip == user_ip) or\
            (email_to and bd.email == email_to) or\
            (message and bd.word and force_text(bd.word) in message):
            bd.rejected_count += 1
            bd.last_rejected = now()
            bd.save()
            return True

    return False


def is_spam(request, form, url):
    """

    """

    data = {
        "user_ip": ip_for_request(request),
        "user_agent": request.META.get("HTTP_USER_AGENT", ""),
        "referrer": request.POST.get("referrer", ""),
        "permalink": url,
    }
    for name, field in form.fields.items():
        data_field = None
        if 'name' in field.label.lower():
            data_field = "name"
        elif isinstance(field, EmailField):
            data_field = "email"
        elif isinstance(field, URLField):
            data_field = "url"
        elif isinstance(field.widget, Textarea):
            data_field = "message"
        if data_field and not data.get(data_field):
            cleaned_data = form.cleaned_data.get(name)
            try:
                data[data_field] = cleaned_data.encode('utf-8')
            except UnicodeEncodeError:
                data[data_field] = cleaned_data
    data['rejected'] = check_spam(data)
    log = SpamLog.objects.create(**data)
    return log.rejected

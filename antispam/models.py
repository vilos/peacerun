from django.db import models
from django.utils.translation import ugettext_lazy as _


class BlacklistDef(models.Model):

    email = models.EmailField(_('Email address'), blank=True, null=True)
    word = models.CharField(_('Word'), max_length=100, blank=True, null=True)
    user_ip = models.CharField(max_length=20, blank=True, null=True)
    rejected_count = models.IntegerField(_('Rejected'), default=0)
    last_rejected = models.DateTimeField(_('Last rejected'), blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Blacklist Definition')
        verbose_name_plural = _('Blacklist Definitions')
        ordering = ('-last_rejected',)

    def __unicode__(self):
        return ' - '.join([x for x in (self.user_ip, self.email, self.word) if x])


class SpamLog(models.Model):

    user_ip = models.CharField(max_length=20, blank=True, null=True)
    user_agent = models.CharField(max_length=255, blank=True, null=True)
    referrer = models.URLField(blank=True, null=True)
    permalink = models.URLField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    rejected = models.BooleanField()

    class Meta:
        verbose_name = _('Spam log')
        verbose_name_plural = _('Spam logs')

    def __unicode__(self):
        return "{} - {}".format(self.permalink, self.user_ip)
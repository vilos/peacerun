from django.contrib import admin
from .models import BlacklistDef, SpamLog


class BlacklistDefAdmin(admin.ModelAdmin):

    #list_display = ('id', 'user_ip', 'email', 'word', 'rejected_count', 'last_rejected')
    list_display = ('__unicode__', 'rejected_count', 'last_rejected')
    readonly_fields = ('rejected_count', 'last_rejected')


admin.site.register(BlacklistDef, BlacklistDefAdmin)


class SpamLogAdmin(admin.ModelAdmin):

    list_display = ('user_ip', 'permalink', 'name', 'email', 'rejected')

    def get_readonly_fields(self, request, obj=None):
        return list(set(
                [field.name for field in self.opts.local_fields]
            ))


admin.site.register(SpamLog, SpamLogAdmin)
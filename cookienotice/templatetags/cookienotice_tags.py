from mezzanine import template
from cookienotice.models import CookieNotice
from countries.utils import current_country_id

register = template.Library()

@register.inclusion_tag("cookienotice/notice_display.html")
def cookienotice():
    notice = None
    qs = CookieNotice.objects.by_country(current_country_id())
    if qs:
        notice = qs[0]
        
    context = {}
    context['cookie_notice_defined'] = False

    if notice:
        context['cookie_notice_defined'] = True
        context['cookie_notice_privacy_url'] = notice.privacy_page_url
        context['cookie_notice_privacy_label'] = notice.privacy_page_label
        context['cookie_notice_button_label'] = notice.button_label
        context['cookie_notice_message'] = notice.message

    return context
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from cookienotice.models import CookieNotice
from countries.utils import current_country_id
from django.forms.models import ModelForm


class CookieNoticeAdminForm(ModelForm):
    class Meta:
        model = CookieNotice

    def clean(self):
        # fill in country automatically
        self.cleaned_data['country_id'] = current_country_id()

        self._validate_unique = True
        return self.cleaned_data
    

class CookieNoticeAdmin(admin.ModelAdmin):
    
    form = CookieNoticeAdminForm
    
    def has_add_permission(self, request):
        num_objects = CookieNotice.objects.by_country(current_country_id()).count()
        # only allow one instance
        if num_objects >= 1:
            return False
        else:
            return True
    
    def queryset(self, request):
        return CookieNotice.objects.by_country(current_country_id())
    

admin.site.register(CookieNotice, CookieNoticeAdmin)

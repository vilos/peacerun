from django.db import models
from django.utils.translation import ugettext_lazy as _
from countries.models import CountryRelated


class CookieNoticeManager(models.Manager):

    def by_country(self, country_id, **kwargs):
        if country_id == '':
            country_id = None
        return self.filter(country_id=country_id, **kwargs)
    
    
class CookieNotice(CountryRelated):
    message = models.CharField(_('Message'), max_length=255, blank=True, null=True, help_text="Translate following text: \"Our site uses cookies. If you continue browsing the site, we assume you agree with our\"")
    privacy_page_url = models.CharField(_('Privacy Page URL'), max_length=255, blank=True, null=True, help_text="URL of localized page with more information about cookies. E.g. /cz/cookies")
    privacy_page_label = models.CharField(_('Privacy Page Label'), max_length=255, blank=True, null=True, help_text="Translate following: \"privacy policy\"")    
    button_label = models.CharField(_('Button Label'), max_length=255, blank=True, null=True, help_text="Translate following: \"Accept\"")
    
    objects = CookieNoticeManager()
    
    def __unicode__(self):
        return u'%s' % self.message
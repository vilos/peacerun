# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'CookieNotice.privacyPageLabel'
        db.delete_column(u'cookienotice_cookienotice', 'privacyPageLabel')

        # Deleting field 'CookieNotice.privacyPageUrl'
        db.delete_column(u'cookienotice_cookienotice', 'privacyPageUrl')

        # Deleting field 'CookieNotice.buttonLabel'
        db.delete_column(u'cookienotice_cookienotice', 'buttonLabel')

        # Adding field 'CookieNotice.privacy_page_url'
        db.add_column(u'cookienotice_cookienotice', 'privacy_page_url',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'CookieNotice.privacy_page_label'
        db.add_column(u'cookienotice_cookienotice', 'privacy_page_label',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'CookieNotice.button_label'
        db.add_column(u'cookienotice_cookienotice', 'button_label',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)


        # Changing field 'CookieNotice.message'
        db.alter_column(u'cookienotice_cookienotice', 'message', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'CookieNotice.privacyPageLabel'
        raise RuntimeError("Cannot reverse this migration. 'CookieNotice.privacyPageLabel' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'CookieNotice.privacyPageLabel'
        db.add_column(u'cookienotice_cookienotice', 'privacyPageLabel',
                      self.gf('django.db.models.fields.CharField')(max_length=255),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'CookieNotice.privacyPageUrl'
        raise RuntimeError("Cannot reverse this migration. 'CookieNotice.privacyPageUrl' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'CookieNotice.privacyPageUrl'
        db.add_column(u'cookienotice_cookienotice', 'privacyPageUrl',
                      self.gf('django.db.models.fields.CharField')(max_length=255),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'CookieNotice.buttonLabel'
        raise RuntimeError("Cannot reverse this migration. 'CookieNotice.buttonLabel' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'CookieNotice.buttonLabel'
        db.add_column(u'cookienotice_cookienotice', 'buttonLabel',
                      self.gf('django.db.models.fields.CharField')(max_length=255),
                      keep_default=False)

        # Deleting field 'CookieNotice.privacy_page_url'
        db.delete_column(u'cookienotice_cookienotice', 'privacy_page_url')

        # Deleting field 'CookieNotice.privacy_page_label'
        db.delete_column(u'cookienotice_cookienotice', 'privacy_page_label')

        # Deleting field 'CookieNotice.button_label'
        db.delete_column(u'cookienotice_cookienotice', 'button_label')


        # User chose to not deal with backwards NULL issues for 'CookieNotice.message'
        raise RuntimeError("Cannot reverse this migration. 'CookieNotice.message' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'CookieNotice.message'
        db.alter_column(u'cookienotice_cookienotice', 'message', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'cookienotice.cookienotice': {
            'Meta': {'object_name': 'CookieNotice'},
            'button_label': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['countries.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'privacy_page_label': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'privacy_page_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'countries.country': {
            'Meta': {'ordering': "['name']", 'object_name': 'Country', 'db_table': "'country'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        }
    }

    complete_apps = ['cookienotice']
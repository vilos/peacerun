# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CookieNotice'
        db.create_table(u'cookienotice_cookienotice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['countries.Country'], null=True, blank=True)),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('privacyPageUrl', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('privacyPageLabel', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('buttonLabel', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'cookienotice', ['CookieNotice'])


    def backwards(self, orm):
        # Deleting model 'CookieNotice'
        db.delete_table(u'cookienotice_cookienotice')


    models = {
        u'cookienotice.cookienotice': {
            'Meta': {'object_name': 'CookieNotice'},
            'buttonLabel': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['countries.Country']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'privacyPageLabel': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'privacyPageUrl': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'countries.country': {
            'Meta': {'ordering': "['name']", 'object_name': 'Country', 'db_table': "'country'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        }
    }

    complete_apps = ['cookienotice']
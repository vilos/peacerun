import os, sys

sys.stdout = sys.stderr

_parent = lambda x: os.path.dirname(x)
PATH_PROJECT = _parent(os.path.abspath(__file__))
PROJECT_PARENT = _parent(PATH_PROJECT)
sys.path.append(PATH_PROJECT)
sys.path.append(PROJECT_PARENT)
PROJECT_NAME = os.path.basename(PATH_PROJECT)

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{0}.settings".format(PROJECT_NAME))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)

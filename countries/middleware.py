from mezzanine.utils.urls import path_to_slug
from models import Country


class CountryMiddleware(object):
    """
    Adds a country id to the request if under a country url.
    """

    def process_request(self, request):
        slug = path_to_slug(request.path_info)
        prefix = slug.split('/')[0]
        if prefix and len(prefix) == 2:
            country = Country.objects.filter(pk=prefix)
            if country:
                country = country[0]
                request.country_id = country.id
                request.country = country
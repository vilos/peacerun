
from django.contrib.admin.views.decorators import staff_member_required
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from utils import set_country_id


@staff_member_required
def set_country(request):
    """
    Put the selected country ID into the session - posted to from
    the "Select country" drop-down in the header of the admin.
    """

    set_country_id(request)
    admin_url = reverse("admin:index")
    next_url = request.GET.get("next", admin_url)
    # Don't redirect to a change view for an object that won't exist
    # on the selected site - go to its list view instead.
    if next_url.startswith(admin_url):
        parts = next_url.split("/")
        if len(parts) > 4 and parts[4].isdigit():
            next_url = "/".join(parts[:4])
    return redirect(next_url)
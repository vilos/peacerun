from django.core.exceptions import PermissionDenied
from mezzanine.core.request import current_request


def set_country_id(request):
    """ put country_id from request data to session """
    from models import CountryPermission

    country_id = request.GET["country_id"]

    if not request.user.is_superuser:
        try:
            CountryPermission.objects.get(user=request.user, countries=country_id)
        except CountryPermission.DoesNotExist:
            raise PermissionDenied

    request.session["country_id"] = country_id


def current_country_id(request=None):
    """
    Responsible for determining the current ``Country`` instance to use
    when retrieving data for any ``CountryRelated`` models. If a request
    is available, and the country can be determined from it, we store the
    country against the request for subsequent retrievals. Otherwise the
    order of checks is as follows:

      - ``country_id`` in session. Used in the admin so that admin users
        can switch sites and stay on the same domain for the admin.

      - host for the current request matched to the domain of the site
        instance.


    """
    if not request:
        request = current_request()
    country_id = getattr(request, "country_id", None)
    if request and not country_id and request.path.startswith('/admin'):
        country_id = request.session.get("country_id", None)
        if not country_id:
            user = request.user
            if user.is_superuser or user.is_anonymous():
                country_id = ''
            else:
                countries = user.countrypermission.countries.all()
                if countries:
                    country = countries[0]
                    country_id = country.id
        if request and country_id is not None:
            request.country_id = country_id
            request.session['country_id'] = country_id
    return country_id

def enabled_countries(request=None):
    from .models import Country
    countries = []
    if not request:
        request = current_request()
    user = request.user
    if not user.is_anonymous():
        countries = Country.objects.filter(permissions__user=user)        
    return countries

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from models import Country, CountryPermission


class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'iso3', 'name', 'active')
    list_editable = ('active',)
    ordering = ('-active', 'name')
    
    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        
        users_with_access = []
        for permission in CountryPermission.objects.get_list_for_country(object_id).all():
            users_with_access.append(permission.user)
        extra_context['users_with_access'] = users_with_access
        
        return super(CountryAdmin, self).change_view(request, object_id,
            form_url, extra_context=extra_context)

admin.site.register(Country, CountryAdmin)

###########################################
# Country Permissions Inlines for User Admin #
###########################################

class CountryPermissionInline(admin.TabularInline):
    model = CountryPermission
    max_num = 1
    can_delete = False


class CountryPermissionUserAdmin(UserAdmin):
    inlines = [CountryPermissionInline]

admin.site.unregister(User)
admin.site.register(User, CountryPermissionUserAdmin)
from django.contrib.auth.models import User
from django.db import models
from django.db.models.loading import get_model
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
from mezzanine.pages.models import Page
from mezzanine.utils.models import base_concrete_model
from utils import current_country_id


class CountryManager(models.Manager):

    def active(self, **kwargs):
        return self.filter(active=True, **kwargs)


class Country(models.Model):
    id = models.CharField('ID', max_length=2, primary_key=True)
    iso2 = models.CharField('ISO 3166-1 Alpha 2 Name', max_length=2, unique=True)
    iso3 = models.CharField('ISO 3166-1 Alpha 3 Name', max_length=3, unique=True)
    name = models.CharField('Country Name', max_length=128, unique=True)
    active = models.BooleanField(default=False)

    objects = CountryManager()

    def __unicode__(self):
        return self.name

    class Meta(object):
        db_table = 'country'
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = ['name']


class CountryPermissionManager(models.Manager):

    def check(self, user, country_id):
        if user.is_anonymous():
            return False        
        if user.is_superuser:
            return True
        return user.is_staff and country_id\
            and self.filter(user=user, countries__id=country_id).exists()
            
    def get_list_for_country(self, country_id):
        return self.filter(countries__id=country_id)


class CountryPermission(models.Model):
    """
    Permission relationship between a user and a country that's
    used instead of ``User.is_staff``, for admin and inline-editing
    access.
    """
    user = models.OneToOneField("auth.User")
    countries = models.ManyToManyField(Country, blank=True, 
                                       related_name='permissions')

    objects = CountryPermissionManager()


def create_country_permission(sender, **kw):
    user = kw["instance"]
    if user.is_staff and not user.is_superuser:
        perm, created = CountryPermission.objects.get_or_create(user=user)

post_save.connect(create_country_permission, sender=User)



class CountryRelatedBase(models.Model):
    """
    Abstract model for all things country-related. Adds a foreignkey to
    Country model, and filters by country with all querysets.
    """

    #objects = CurrentCountryManager()

    class Meta:
        abstract = True

    def save(self, update_country=False, *args, **kwargs):
        """
        Set the current country when the record is first
        created, or the ``update_country`` argument is explicitly set
        to ``True``.
        """
        if update_country or not self.id:
            if not self.country_id:
                self.country_id = current_country_id()

        # root country page should have the same slug as country id
        if hasattr(self, 'parent') and not self.parent and self.country_id:
            try:
                root = Page.objects.get(slug=self.country_id)
            except Page.DoesNotExist:
                self.slug = self.country_id
            else:
                root = root.get_content_model()
                if root != self:
                    # put the current page under the root
                    self.parent = root
                    if not self.slug.startswith(root.slug):
                        self.slug = '/'.join([root.slug, self.slug])
                        
        super(CountryRelatedBase, self).save(*args, **kwargs)

    def get_country_id(self):
        country_id = getattr(self, 'country_id', '') 
        return country_id if country_id else ''

    def is_editable(self, request):
        country_id = getattr(self.country, 'id', None)
        return CountryPermission.objects.check(request.user, country_id)
        


class CountryRelated(CountryRelatedBase):
    """ abstract model, adds an optional relation to country model """

    country = models.ForeignKey(Country, editable=False, blank=True, null=True)

    class Meta:
        abstract = True

class CountryRelatedRequired(CountryRelatedBase):

    country = models.ForeignKey(Country)

    class Meta:
        abstract = True

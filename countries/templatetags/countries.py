from mezzanine import template
from mezzanine.core.templatetags.mezzanine_tags import admin_app_list
from ..models import Country, CountryPermission
from ..utils import current_country_id

register = template.Library()


@register.inclusion_tag("admin/includes/dropdown_menu.html",
                        takes_context=True)
def admin_dropdown_menu(context):
    """
    Renders the app list for the admin dropdown menu navigation.
    """
    context["dropdown_menu_app_list"] = admin_app_list(context["request"])
    # disabling sites dropdown
    context["dropdown_menu_sites"] = []

    user = context["request"].user
    if user.is_superuser:
        context["dropdown_menu_countries"] = list(Country.objects.active())
    else:
        context["dropdown_menu_countries"] = list(user.countrypermission.countries.all())

    context["current_country_id"] = current_country_id()
    return context

@register.inclusion_tag("countries/select_country.html", takes_context=True)
def select_country(context):
    context["countries"] = list(Country.objects.active())
    context["current_country_id"] = current_country_id(context['request'])
    return context

@register.filter(name='permitted_path') 
def permitted_path(user, path):
    parts =  path.split('/')
    if parts[0] == 'reports':
        parts = parts[1:]
    country_id = None
    if parts:        
        country_id = parts[0]
    if country_id:
        return CountryPermission.objects.check(user, country_id)
    # superuser can always edit root directory
    return user.is_superuser

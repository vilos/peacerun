"""
    from http://djangosnippets.org/snippets/1049/
"""
import urllib, codecs
from django.core.management.base import BaseCommand
from countries.models import Country

COUNTRY_INFO_URL = "http://download.geonames.org/export/dump/countryInfo.txt"


def get_geonames_country_data():
    "Returns a list of dictionaries, each representing a country"
    udata = urllib.urlopen(COUNTRY_INFO_URL).read().decode('utf8')
    # Strip the BOM
    if udata[0] == codecs.BOM_UTF8.decode('utf8'):
        udata = udata[1:]
    # Ignore blank lines
    lines = [l for l in udata.split('\n') if l]
    # Find the line with the headers (starts #ISO)
    header_line = [l for l in lines if l.startswith('#ISO')][0]
    headers = header_line[1:].split('\t')
    # Now get all the countries
    country_lines = [l for l in lines if not l.startswith('#')]
    #countries = []
    for line in country_lines:
        #countries.append(dict(zip(headers, line.split('\t'))))
        yield dict(zip(headers, line.split('\t')))
    #return countries

class Command(BaseCommand):

    def handle(self, *args, **options):

        qs = Country.objects.all()
        for line in get_geonames_country_data():
            iso2 = line['ISO']
            data = dict(iso2 = iso2,
                        iso3 = line['ISO3'],
                        name = line['Country'])
            pk = iso2.lower()
            obj, created = qs.get_or_create(id=pk, defaults=data)

            if not created:
                qs.filter(pk=obj.pk).update(**data)

from django.shortcuts import get_object_or_404, get_list_or_404
from django.views.generic import TemplateView
from models import Update

class DownloadView(TemplateView):
    template_name = "runporter/download.html"

    def get_context_data(self, **kwargs):
        update = Update.objects.get_latest()
        return {'update' : update}


class UpdateXMLView(TemplateView):
    template_name = "runporter/update.xml"

    def get_context_data(self, **kwargs):
        update = Update.objects.get_latest()
        req = self.request
        file_url = "{0}://{1}{2}".format(req.environ['wsgi.url_scheme'],
                                         req.environ['SERVER_NAME'], update.file.url)

        return {'update' : update, 'file_url': file_url}


class MinimalXMLView(TemplateView):
    template_name = "runporter/minimal.xml"

    def get_context_data(self, **kwargs):
        objs = get_list_or_404(Update, is_minimal=True)
        obj = objs[0]
        return {'update' : obj}
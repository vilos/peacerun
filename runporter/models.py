from django.db import models
from django.utils.translation import ugettext_lazy as _
from fields import VersionField


class UpdateManager(models.Manager):

    def get_latest(self, **kwargs):
        updates =  self.all()
        if updates:
            return updates[0]


class Update(models.Model):
    number = VersionField(verbose_name=_('Version number'))
    label  = models.CharField(_("Version label"), max_length=255)
    description = models.TextField(_('Description'), blank=True, null=True)
    is_minimal = models.BooleanField(_('Minimally required'), default=False)
    file = models.FileField(_('Application file'), upload_to="runporter")

    objects = UpdateManager()

    class Meta:
        verbose_name = "Update"
        ordering = ('-number',)

    def __unicode__(self):
        return unicode(self.number)


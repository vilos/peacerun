from django.conf.urls import patterns, url
from views import DownloadView, UpdateXMLView, MinimalXMLView

urlpatterns = patterns('',
    url(r'^$', DownloadView.as_view(), name='download'),
    url(r'^update.xml$', UpdateXMLView.as_view(), name='last-update'),
    url(r'^updateMinVersion.xml/?$', MinimalXMLView.as_view(), name='minimal-version'),

    )
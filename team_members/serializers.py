from rest_framework import serializers
from countries.models import Country
from models import TeamMember


class TeamSerializer(serializers.ModelSerializer):

    country = serializers.PrimaryKeyRelatedField(many=False, queryset=Country.objects.all())

    class Meta:
        model = TeamMember
        fields = ('id', 'name', 'surname', 'country')
        read_only_fields = ('id',)
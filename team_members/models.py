from django.db import models
from django.utils.translation import ugettext_lazy as _


class TeamMemberManager(models.Manager):

    def reports_run(self, country_id, date, **kwargs):
        return self.filter(reports_run__country__id=country_id, reports_run__date=date, **kwargs).distinct()

    def reports_written(self, country_id, date, **kwargs):
        return self.filter(reports_written__country__id=country_id, reports_written__date=date, **kwargs).distinct()

    def reports_pictured(self, country_id, date, **kwargs):
        return self.filter(reports_pictured__country__id=country_id, reports_pictured__date=date, **kwargs).distinct()


class TeamMember(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    surname = models.CharField(_('Surname'), max_length=255)
    country = models.ForeignKey('countries.Country')
    created  = models.DateTimeField(_('Created'), auto_now_add=True)
    last_modified = models.DateTimeField(_('Last modified'), auto_now=True)
    active = models.BooleanField(_('Active'), default=True)

    objects = TeamMemberManager()

    class Meta(object):
        db_table = 'team_member'
        verbose_name = _('Team member')
        verbose_name_plural = _('Team members')
        ordering = ['name', 'surname']
        unique_together = ('name', 'surname', 'country')

    def __unicode__(self):
        return u"{0} {1}, {2}".format(self.name, self.surname, self.country_id.upper())

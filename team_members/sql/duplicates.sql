﻿select t1.id, t1.country_id,  t1.name,  t1.surname,
(select count(id) from reports_report_runners where teammember_id=t1.id) reports_run,
(select count(id) from reports_report_authors where teammember_id=t1.id) reports_written,
(select count(id) from reports_report_photographers where teammember_id=t1.id) reports_pictured
from team_member t1
--join team_member t2 on t1.country_id=t2.country_id and t1.name=t2.name and t1.surname=t2.surname and t1.id<>t2.id
where
 exists (
	select id  from team_member t2
	where t1.country_id=t2.country_id and t1.name=t2.name and t1.surname=t2.surname and t1.id<>t2.id
 )
order by t1.id
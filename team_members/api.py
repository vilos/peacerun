from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from serializers import TeamSerializer
from models import TeamMember


class TeamList(generics.ListCreateAPIView):
    model = TeamMember
    serializer_class = TeamSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def create(self, request, *args, **kwargs):
        data=request.DATA
        serializer = self.get_serializer(data=data, files=request.FILES)

        if serializer.is_valid():
            new = serializer.object
            objs = TeamMember.objects.filter(name=new.name, surname=new.surname, country=new.country)
            if objs:
                data = {'id' : objs[0].pk}
                return Response(data, status=status.HTTP_200_OK)


            self.object = serializer.save(force_insert=True)
            data = {'id' : self.object.pk}
            return Response(data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamMemberDetail(generics.RetrieveAPIView):
    model = TeamMember
    serializer_class = TeamSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
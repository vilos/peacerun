from django.contrib import admin
from models import TeamMember


class TeamMemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'country', 'active')


admin.site.register(TeamMember, TeamMemberAdmin)
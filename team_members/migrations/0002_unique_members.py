# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'TeamMember', fields ['country', 'surname', 'name']
        db.create_unique('team_member', ['country_id', 'surname', 'name'])


    def backwards(self, orm):
        # Removing unique constraint on 'TeamMember', fields ['country', 'surname', 'name']
        db.delete_unique('team_member', ['country_id', 'surname', 'name'])


    models = {
        u'countries.country': {
            'Meta': {'ordering': "['-active', 'name']", 'object_name': 'Country', 'db_table': "'country'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'iso2': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'iso3': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'team_members.teammember': {
            'Meta': {'ordering': "['name', 'surname']", 'unique_together': "(('name', 'surname', 'country'),)", 'object_name': 'TeamMember', 'db_table': "'team_member'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['countries.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['team_members']
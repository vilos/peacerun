import logging
from django.core.management.base import BaseCommand
from ...models import TeamMember

log = logging.getLogger('merge_members')


class Command(BaseCommand):

    def handle(self, *args, **options):
        dups = []
        for master in TeamMember.objects.all().order_by('pk'):
            if master.pk in dups:
                continue
            for dup in TeamMember.objects.filter(name=master.name,
                                                 surname=master.surname,
                                                 country=master.country).exclude(pk=master.pk):
                dups.append(dup.pk)
                for r in dup.reports_run.all():
                    log.info('report: {0}, runner: {1} -> {2}'.format(r, dup.pk, master.pk))
                    r.runners.remove(dup)
                    r.runners.add(master)

                for r in dup.reports_written.all():
                    log.info('report: {0}, author: {1} -> {2}'.format(r, dup.pk, master.pk))
                    r.authors.remove(dup)
                    r.authors.add(master)

                for r in dup.reports_pictured.all():
                    log.info('report: {0}, photographer {1} -> {2}'.format(r, dup.pk, master.pk))
                    r.photographers.remove(dup)
                    r.photographers.add(master)

        for pk in dups:
            tm = TeamMember.objects.get(pk=pk)
            if tm.reports_run.count() == 0 and tm.reports_written.count()==0 and tm.reports_pictured.count() == 0:
                log.info('deleting duplicite member: {0}'.format(tm))
                tm.delete()
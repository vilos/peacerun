# coding: utf-8
######################
# MEZZANINE SETTINGS #
######################

# The following settings are already defined with default values in
# the ``defaults.py`` module within each of Mezzanine's apps, but are
# common enough to be put here, commented out, for convenient
# overriding. Please consult the settings documentation for a full list
# of settings Mezzanine implements:
# http://mezzanine.jupo.org/docs/configuration.html#default-settings

# Controls the ordering and grouping of the admin menu.
#
# ADMIN_MENU_ORDER = (
#     ("Content", ("pages.Page", "blog.BlogPost",
#        "generic.ThreadedComment", ("Media Library", "fb_browse"),)),
#     ("Site", ("sites.Site", "redirects.Redirect", "conf.Setting")),
#     ("Users", ("auth.User", "auth.Group",)),
# )
ADMIN_MENU_ORDER = (
    ("Content", ("pages.Page",
                 "mezzanine_blocks.RichBlock",
                 "reports.Report",
                 "team_members.TeamMember",
                 ("Media Library", "fb_browse"),
                 ("Link Checker", "linkcheck_report"),
                 ("Cookie Notice", "cookienotice.CookieNotice"),
                 )),
    ("Site", ("sites.Site", "redirects.Redirect", "conf.Setting",
              "antispam.BlacklistDef", "antispam.SpamLog")),
    ("Users", ("auth.User", "auth.Group",)),
)
# A three item sequence, each containing a sequence of template tags
# used to render the admin dashboard.
#
# DASHBOARD_TAGS = (
#     ("blog_tags.quick_blog", "mezzanine_tags.app_list"),
#     ("comment_tags.recent_comments",),
#     ("mezzanine_tags.recent_actions",),
# )
DASHBOARD_TAGS = (
    ("mezzanine_tags.app_list",),
    ("mezzanine_tags.recent_actions",),
    ()
)

# A sequence of templates used by the ``page_menu`` template tag. Each
# item in the sequence is a three item sequence, containing a unique ID
# for the template, a label for the template, and the template path.
# These templates are then available for selection when editing which
# menus a page should appear in. Note that if a menu template is used
# that doesn't appear in this setting, all pages will appear in it.

# PAGE_MENU_TEMPLATES = (
#     (1, "Top navigation bar", "pages/menus/dropdown.html"),
#     (2, "Left-hand tree", "pages/menus/tree.html"),
#     (3, "Footer", "pages/menus/footer.html"),
# )

PAGE_MENU_TEMPLATES = (
    (1, "Top navigation bar", "pages/menus/primary.html"),
    (2, "Right-hand tree", "pages/menus/subtree.html"),
    #(3, "Footer", "pages/menus/footer.html"),
)
# A sequence of fields that will be injected into Mezzanine's (or any
# library's) models. Each item in the sequence is a four item sequence.
# The first two items are the dotted path to the model and its field
# name to be added, and the dotted path to the field class to use for
# the field. The third and fourth items are a sequence of positional
# args and a dictionary of keyword args, to use when creating the
# field instance. When specifying the field class, the path
# ``django.models.db.`` can be omitted for regular Django model fields.
#
# EXTRA_MODEL_FIELDS = (
#     (
#         # Dotted path to field.
#         "mezzanine.blog.models.BlogPost.image",
#         # Dotted path to field class.
#         "somelib.fields.ImageField",
#         # Positional args for field class.
#         ("Image",),
#         # Keyword args for field class.
#         {"blank": True, "upload_to": "blog"},
#     ),
#     # Example of adding a field to *all* of Mezzanine's content types:
#     (
#         "mezzanine.pages.models.Page.country",
#         "ForeignKey", # 'django.db.models.' is implied if path is omitted.
#         ("countries.Country",), {"null": True}
#     ),
# )

# Setting to turn on featured images for blog posts. Defaults to False.
#
# BLOG_USE_FEATURED_IMAGE = True

# If True, the south application will be automatically added to the
# INSTALLED_APPS setting.
USE_SOUTH = True

# disable admin for comments
COMMENTS_DISQUS_SHORTNAME = ' '

########################
# MAIN DJANGO SETTINGS #
########################

# People who get code error notifications.
# In the format (('Full Name', 'email@example.com'),
#                ('Full Name', 'anotheremail@example.com'))
ADMINS = (
    ('Viliam Segeda', 'v.segeda@madalbalstudio.com'),
    #('Ondrej Mocny', 'ondrej.mocny@gmail.com')
)
MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = None

# If you set this to True, Django will use timezone-aware datetimes.
USE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en"

# A boolean that turns on/off debug mode. When set to ``True``, stack traces
# are displayed for error pages. Should always be set to ``False`` in
# production. Best set to ``True`` in local_settings.py
DEBUG = False
TEMPLATE_DEBUG = DEBUG

# Whether a user's session cookie expires when the Web browser is closed.
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# Tuple of IP addresses, as strings, that:
#   * See debug comments, when DEBUG is true
#   * Receive x-headers
INTERNAL_IPS = ("127.0.0.1",)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)

AUTHENTICATION_BACKENDS = ("mezzanine.core.auth_backends.MezzanineBackend", "auth.SuperuserLoginAuthenticationBackend")

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
)


#############
# DATABASES #
#############

DATABASES = {
    "default": {
        "ENGINE": "django_postgrespool",
        "NAME": "peacerun",
        "USER": "django",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}

DATABASE_POOL_ARGS = {
    'max_overflow': 150,
    'pool_size': 2,
    'recycle': 300
}

SOUTH_DATABASE_ADAPTERS = {
    'default': 'south.db.postgresql_psycopg2'
}

#########
# PATHS #
#########

import os

# Full filesystem path to the project.
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# Name of the directory for the project.
PROJECT_DIRNAME = PROJECT_ROOT.split(os.sep)[-1]

# Every cache key will get prefixed with this value - here we set it to
# the name of the directory the project is in to try and use something
# project specific.
CACHE_MIDDLEWARE_KEY_PREFIX = PROJECT_DIRNAME

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = "/static/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, STATIC_URL.strip("/"))

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = STATIC_URL + "media/"

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, "assets"),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, *MEDIA_URL.strip("/").split("/"))

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = STATIC_URL + "grappelli/"

# Package/module name to import the root urlpatterns from for the project.
ROOT_URLCONF = "%s.urls" % PROJECT_DIRNAME

# Put strings here, like "/home/html/django_templates"
# or "C:/www/django/templates".
# Always use forward slashes, even on Windows.
# Don't forget to use absolute paths, not relative paths.
TEMPLATE_DIRS = (os.path.join(PROJECT_ROOT, "templates"),)


################
# APPLICATIONS #
################

INSTALLED_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.markup",
    "django.contrib.redirects",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.sitemaps",
    "django.contrib.staticfiles",
    "mezzanine.boot",
    "mezzanine.conf",
    "mezzanine.core",
    "mezzanine.generic",
    #"mezzanine.blog",
    "mezzanine.forms",
    "mezzanine.pages",
    #"mezzanine.galleries",
    #"mezzanine.twitter",
    "mezzanine.accounts",
    "mezzanine.mobile",
    'mezzanine_blocks',
    'mezzanine_slides',
    'south',
    'countries',
    'country_pages',
    'reports',
    'team_members',
    'rest_framework',
    'runporter',
    'post_office',
    'geoposition',
    'dbbackup',
    'django_rq',
    'easy_thumbnails',
    #'taggit',
    'admin_notifications',
    'linkcheck',
    'cookienotice',
    'antispam'
)

# List of processors used by RequestContext to populate the context.
# Each one should be a callable that takes the request object as its
# only parameter and returns a dictionary to add to the context.
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.static",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.tz",
    "mezzanine.conf.context_processors.settings",
)

# List of middleware classes to use. Order is important; in the request phase,
# these middleware classes will be applied in the order given, and in the
# response phase the middleware will be applied in reverse order.
MIDDLEWARE_CLASSES = (
    "mezzanine.core.middleware.UpdateCacheMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.redirects.middleware.RedirectFallbackMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "mezzanine.core.request.CurrentRequestMiddleware",
    "mezzanine.core.middleware.TemplateForDeviceMiddleware",
    "mezzanine.core.middleware.TemplateForHostMiddleware",
    "mezzanine.core.middleware.AdminLoginInterfaceSelectorMiddleware",
    "mezzanine.core.middleware.SitePermissionMiddleware",
    # Uncomment the following if using any of the SSL settings:
    # "mezzanine.core.middleware.SSLRedirectMiddleware",
    'countries.middleware.CountryMiddleware',
    "mezzanine.pages.middleware.PageMiddleware",
    "mezzanine.core.middleware.FetchFromCacheMiddleware",
)

# Store these package names here as they may change in the future since
# at the moment we are using custom forks of them.
PACKAGE_NAME_FILEBROWSER = "filebrowser_safe"
PACKAGE_NAME_GRAPPELLI = "grappelli_safe"

# Linkcheck setup
SITE_DOMAIN = "peacerun.org"
LINKCHECK_MEDIA_PREFIX = "/static/media/"

#########################
# OPTIONAL APPLICATIONS #
#########################

# These will be added to ``INSTALLED_APPS``, only if available.
OPTIONAL_APPS = (
    "debug_toolbar",
    "django_extensions",
    "compressor",
    PACKAGE_NAME_FILEBROWSER,
    PACKAGE_NAME_GRAPPELLI,
    "admin_notifications",
)

TINYMCE_SETUP_JS = "%s/js/tinymce_setup.js" % STATIC_URL

DEBUG_TOOLBAR_CONFIG = {"INTERCEPT_REDIRECTS": False}

COMPRESS_PRECOMPILERS = (
   ('text/less', 'lessc {infile} {outfile}'),
)

###################
# THUMBNAILS      #
###################

from easy_thumbnails.conf import Settings as easy_thumbnails_defaults

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

THUMBNAIL_ALIASES = {
    '': {
        'report_main' : {'size': (450, 0),  'crop': False },
        'gallery_thumb':{'size': (70, 70),  'crop': True },
        'report_admin_thumb': {'size': (70, 70),  'crop': True },
        # these names must correspond to their usage in report_gallery.html
        'screen_small' : {'size': (450, 0),  'crop': False },
    },
}
THUMBNAIL_PROCESSORS = (
    'reports.thumbnail_processors.crop_rect',
) + easy_thumbnails_defaults.THUMBNAIL_PROCESSORS
REPORT_THUMBNAIL_SIZE = (200, 115) # size of the thumbnail displayed next to report when listing reports
REPORT_VIDEO_SIZE = (450, 300)
THUMBNAIL_BASEDIR = 'thumbs'
GALLERY_SCREEN_SIZES = [THUMBNAIL_ALIASES[''][k]['size'][0] for k in THUMBNAIL_ALIASES[''] if k.startswith('screen')]
GALLERY_SCREEN_SIZES.append(1600) # this is max size of the original image
GALLERY_SCREEN_SIZES.sort()
#THUMBNAIL_SUBDIR = '.thumbnails'

# https://github.com/SmileyChris/easy-thumbnails/issues/223
# The following fixes the PIL "Suspension not allowed here" error
# 1048576 Bytes = 1 MB
try:
    from PIL import ImageFile
except ImportError:
    import ImageFile
ImageFile.MAXBLOCK = 1048576

###################
# EMAIL SETTINGS  #
###################

EMAIL_BACKEND = 'post_office.EmailBackend'

# fixing email from header
FORMS_DISABLE_SEND_FROM_EMAIL_FIELD = True

###################
# form settings   #
###################

FORMS_UPLOAD_ROOT = MEDIA_ROOT
FILE_UPLOAD_PERMISSIONS = 0644

# add captcha field to forms
FORMS_EXTRA_FIELDS = [
    [16, 'simplemathcaptcha.fields.MathCaptchaField', 'Captcha']
]

SPAM_FILTERS = (
        'antispam.filters.is_spam',
    )
###################
# redis
###################

RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0,
        'ASYNC' : True
    },
    'high': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0,
        'ASYNC' : True
    },
}

RQ_SHOW_ADMIN_LINK = True


###################
# reports
###################

LOCAL_NEWS_DISPLAY_COUNT = 4
GLOBAL_NEWS_DISPLAY_COUNT = 6
GLOBAL_NEWS_CHANGER_COUNT = 10


###################
# DEPLOY SETTINGS #
###################

# These settings are used by the default fabfile.py provided.
# Check fabfile.py for defaults.

FABRIC = {
    "SSH_USER": "django", # SSH username
    "SSH_PASS":  "", # SSH password (consider key-based authentication)
    "SSH_KEY_PATH":  "~/.ssh/django/id_rsa_django.pub", # Local path to SSH key file, for key-based auth
    "HOSTS": ['peacerun'], # List of hosts to deploy to
    "VIRTUALENV_HOME":  "/home/django/ve/", # Absolute remote path for virtualenvs
    "PROJECT_NAME": "peacerun", # Unique identifier for project
    "REQUIREMENTS_PATH": "requirements/project.txt", # Path to pip requirements, relative to project
    "GUNICORN_PORT": 8000, # Port gunicorn will listen on
    "LOCALE": "en_US.UTF-8", # Should end with ".UTF-8"
    "LIVE_HOSTNAME": "peacerun.org", # Host for public site.
    "REPO_URL": "ssh://hg@bitbucket.org/vilos/peacerun", # Git or Mercurial remote repo URL for the project
    "DB_PASS": "", # Live database password
    "ADMIN_PASS": "", # Live admin user password
}

UPLOAD_TO_HANDLERS = {
    #'mezzanine_slides.Slide.file' : 'countries.utils.current_country_id'
    'reports.ReportImage.file' : 'reports.utils.get_folder'
}

FILEBROWSER_DEFAULT_SORTING_BY = 'filename_lower'
FILEBROWSER_DEFAULT_SORTING_ORDER = 'asc'
FILEBROWSER_EXTENSIONS = {
    'Folder': [''],
    'Image': ['.jpg', '.jpeg', '.gif', '.png', '.tif', '.tiff'],
    'Document': ['.pdf', '.doc', '.odt', '.rtf', '.txt', '.xls', '.csv'],
    'Audio': ['.mp3', '.mp4', '.wav', '.aiff', '.midi', '.m4p'],
    'Video': ['.mov', '.wmv', '.mpeg', '.mpg', '.avi', '.rm'],
    'Archive': ['.zip'],
    'Code': ['.html', '.py', '.js', '.css'],
}
FILEBROWSER_SELECT_FORMATS = {
    'File': ['Folder', 'Document'],
    'Image': ['Image'],
    'Media': ['Video', 'Audio'],
    'Document': ['Document'],
    # for TinyMCE we can also define lower-case items
    'image': ['Image'],
    'file': ['Folder', 'Image', 'Document', 'Archive'],
    'media': ['Video', 'Audio'],
}
FILEBROWSER_LIST_PER_PAGE = 100

##################
# BACKUP         #
##################

DBBACKUP_POSTGRESQL_DB_ENGINES = ('django_postgrespool', 'postgresql_psycopg2')
DBBACKUP_STORAGE = 'dbbackup.storage.filesystem_storage'
DBBACKUP_FILESYSTEM_DIRECTORY = os.path.join(os.environ.get('VIRTUAL_ENV','~'), 'var', 'backups')


SITE_READ_ONLY = False

##################
# LOCAL SETTINGS #
##################

# Allow any settings to be defined in local_settings.py which should be
# ignored in your version control system allowing for settings to be
# defined per machine.
try:
    from env import *
except ImportError:
    pass

try:
    from local_settings import *
except ImportError:
    pass

try:
    from logging_settings import *
except ImportError:
    pass

####################
# DYNAMIC SETTINGS #
####################

# set_dynamic_settings() will rewrite globals based on what has been
# defined so far, in order to provide some better defaults where
# applicable. We also allow this settings module to be imported
# without Mezzanine installed, as the case may be when using the
# fabfile, where setting the dynamic settings below isn't strictly
# required.
try:
    from mezzanine.utils.conf import set_dynamic_settings
except ImportError:
    pass
else:
    set_dynamic_settings(globals())

GRAPPELLI_ADMIN_HEADLINE = 'Peace Run'
GRAPPELLI_ADMIN_TITLE = 'Peace Run'

if SITE_READ_ONLY:
    INSTALLED_APPS += ('readonly',)
    MIDDLEWARE_CLASSES += ('readonly.middleware.ReadOnlySiteMiddleware',)
    READ_ONLY_PATH_STARTS = ('/admin/', '/api/', '/api-auth/')



from django.db import models
from django.utils.translation import ugettext_lazy as _
from mezzanine.core.fields import FileField
from mezzanine.core.models import RichText
from mezzanine.pages.models import Page
from mezzanine.forms.models import Form
from mezzanine.utils.models import upload_to
from countries.models import CountryRelated


class CountryPage(Page, RichText, CountryRelated):
    """
    Implements the default type of page with a single Rich Text
    content field.
    """

    class Meta:
        db_table = 'country_page'
        verbose_name = _("Rich text page")
        verbose_name_plural = _("Rich text pages")

    def get_slides(self):
        slides = None
        if hasattr(self, 'slide_set'):
            slides = self.slide_set.all()

        if not slides and self.parent and self.country_id:
            root = CountryPage.objects.get(parent=None, slug=self.country_id)
            slides = root.get_slides()

        if not slides:
            pages = CountryPage.objects.filter(parent=None, country=None)
            if pages:
                slides = pages[0].get_slides()
        return slides

    def can_add(self, request):
        return self.slug != "/" and self.is_editable(request)

    def can_change(self, request):
        return self.is_editable(request)

    def can_delete(self, request):
        return self.is_editable(request)
    
    
class CountryLink(Page, CountryRelated):

    class Meta:
        db_table = 'country_link'
        verbose_name = _("Link")
        verbose_name_plural = _("Links")


    def get_absolute_url(self):
        """
        simply return slug, handle internal links
        """
        slug = self.slug

        # Ensure the URL is absolute.
        if not slug.lower().startswith("http"):
            slug = "/" + self.slug.lstrip("/")
        return slug

    def can_add(self, request):
        return self.slug != "/" and self.is_editable(request)

    def can_change(self, request):
        return self.is_editable(request)

    def can_delete(self, request):
        return self.is_editable(request)


class CountryForm(Form, CountryRelated):
    
    class Meta:
        db_table = "country_form"
        verbose_name = _("Form")
        verbose_name_plural = _("Forms")

    def can_add(self, request):
        return self.slug != "/" and self.is_editable(request)

    def can_change(self, request):
        return self.is_editable(request)

    def can_delete(self, request):
        return self.is_editable(request)
    

class RouteMap(models.Model):
    
    map_image = FileField(_("Map Image"), max_length=255, format="Image",
                          upload_to=upload_to("reports.ReportImage.file", "reports"), 
                          blank=True, null=True)

    class Meta:
        abstract = True  


class CountryMap(Page, RouteMap, RichText, CountryRelated):
    
    class Meta:
        db_table = 'country_route'
        verbose_name = _("Route Map")
        verbose_name_plural = _("Route Map")

    def can_add(self, request):
        return self.slug != "/" and self.is_editable(request)

    def can_change(self, request):
        return self.is_editable(request)

    def can_delete(self, request):
        return self.is_editable(request)
    
    
class RoutePoint(models.Model):
    map  = models.ForeignKey(CountryMap, related_name="points")
    name = models.CharField(_('Name'), max_length=255)
    dates = models.CharField(_('Dates'), max_length=255)
    url = models.CharField(_('URL'), max_length=255, blank=True, null=True)
    x = models.PositiveIntegerField(_('Map image position x'))
    y = models.PositiveIntegerField(_('Map image position y'))
    
    class Meta:
        db_table = 'country_routepoints'
        verbose_name = _("Route Point")
        verbose_name_plural = _("Route Points")
            
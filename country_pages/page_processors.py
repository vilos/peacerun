from django.shortcuts import redirect
from django.template import RequestContext
from django.utils.encoding import filepath_to_uri
from mezzanine.conf import settings
from mezzanine.forms.forms import FormForForm
from mezzanine.forms.signals import form_invalid, form_valid
from mezzanine.pages.page_processors import processor_for
from mezzanine.utils.email import split_addresses, send_mail_template
from mezzanine.utils.views import is_spam
from mezzanine.forms.page_processors import format_value
from models import CountryForm


@processor_for(CountryForm)
def form_processor(request, page):
    """
    Display a built form and handle submission.
    """
    form = FormForForm(page.form, RequestContext(request),
                       request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            url = page.get_absolute_url()
            if is_spam(request, form, url):
                url += "?spam=1"
                return redirect(url)

            url += "?sent=1"

            entry = form.save()
            subject = page.form.email_subject
            if not subject:
                subject = "%s - %s" % (page.form.title, entry.entry_time)

            fields = []
            for k, field in form.fields.items():
                data = form.cleaned_data[k]
                if data:
                    if field.type == 'filefield':
                        pk = int(k.replace('field_',''))
                        fieldentry = entry.fields.get(field_id=pk)
                        value = 'http://' + request.get_host() + settings.MEDIA_URL + filepath_to_uri(fieldentry.value)
                    else:
                        value = format_value(form.cleaned_data[k])
                    fields.append((field.label, value))
            context = {
                "fields": fields,
                "message": page.form.email_message,
                "request": request,
            }
            email_from = page.form.email_from or settings.DEFAULT_FROM_EMAIL
            email_to = form.email_to()
            if email_to and page.form.send_email:
                send_mail_template(subject, "email/form_response", email_from,
                                   email_to, context)
            headers = None
            if email_to:
                # Add the email entered as a Reply-To header
                headers = {'Reply-To': email_to}
            email_copies = split_addresses(page.form.email_copies)
            if email_copies:
                send_mail_template(subject, "email/form_response_copies",
                                   email_from, email_copies, context,
                                   headers=headers)
            form_valid.send(sender=request, form=form, entry=entry)
            return redirect(url)
        form_invalid.send(sender=request, form=form)
    elif request.method == 'GET':
        # disable caching
        request._update_cache = False
    return {"form": form}
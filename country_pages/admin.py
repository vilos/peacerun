from django.contrib import admin
from mezzanine.pages.admin import PageAdmin, LinkAdmin
from mezzanine.pages.models import Page, RichTextPage, Link
from mezzanine.forms.models import Form
from mezzanine.forms.admin import FormAdmin
from mezzanine.core.admin import TabularDynamicInlineAdmin
from mezzanine_slides.admin import SlideInline
from models import CountryPage, CountryLink, CountryForm, CountryMap, RoutePoint

class CountryPageAdmin(PageAdmin):
    inlines = (SlideInline,)

class CountryLinkAdmin(LinkAdmin):
    pass

class RoutePointInline(TabularDynamicInlineAdmin):
    model = RoutePoint
    
class CountryMapAdmin(PageAdmin):
    inlines = (RoutePointInline,)

admin.site.unregister(Page)
admin.site.unregister(RichTextPage)
admin.site.unregister(Link)
admin.site.unregister(Form)
admin.site.register(Page, CountryPageAdmin)
admin.site.register(CountryPage, CountryPageAdmin)
admin.site.register(CountryLink, CountryLinkAdmin)
admin.site.register(CountryForm, FormAdmin)
admin.site.register(CountryMap, CountryMapAdmin)
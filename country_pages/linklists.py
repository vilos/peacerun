from linkcheck import Linklist
from country_pages.models import CountryPage
from mezzanine.core.models import CONTENT_STATUS_PUBLISHED

class CountryPageLinklist(Linklist):
    model = CountryPage
    object_filter = {'status': CONTENT_STATUS_PUBLISHED}
    html_fields = ['content', 'description']
        
linklists = {'Country Pages': CountryPageLinklist}
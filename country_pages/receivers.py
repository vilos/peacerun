import os
from django.conf import settings
from django.dispatch import receiver
from filebrowser_safe.views import filebrowser_post_upload
                

@receiver(filebrowser_post_upload)
def file_upload(sender, **kwargs):
    
    fobj = kwargs.get('file', None)
    if fobj:
        path = os.path.join(settings.MEDIA_ROOT, fobj.path)    
        if os.path.exists(path):
            os.chmod(path, 0644)

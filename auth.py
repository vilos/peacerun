from django.contrib.auth import authenticate as django_authenticate
from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend


class SuperuserLoginAuthenticationBackend(ModelBackend):
    """ Let superusers login as regular users using name:  superuser:user and usual superusers password """
    LOGIN_SEPARATOR = ':'

    def authenticate(self, username=None, password=None):
        if self.LOGIN_SEPARATOR not in username:
            return None

        supername, username = username.split(self.LOGIN_SEPARATOR, 1)

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return None

        superuser = django_authenticate(username=supername, password=password)
        if superuser and (superuser.is_superuser or superuser.is_staff):
            return user

from django.core.management.base import BaseCommand
from linkcheck.models import Url, Link


class Command(BaseCommand):
    help = "Resets all stored links"
    def execute(self, *args, **options):
        print "Reseting all stored links"
        Url.objects.all().delete()
        Link.objects.all().delete()

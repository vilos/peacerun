from fabric.api import env, task
#import ssh
#ssh.util.log_to_file("paramiko.log", 10)
from fabdeploy.server import *
from fabdeploy.django import *

# globals
env.colors = True
env.use_ssh_config = True
env.locale = 'en_US.UTF-8'
env.django_user = 'django'
env.django_user_key = "~/.ssh/django/id_rsa_django.pub"
env.django_private_key = "~/.ssh/django/id_rsa_django"


env.workon_home = 'venv'
env.virtual_env = 'peacerun'
env.project = 'peacerun'
env.repo = 'ssh://hg@bitbucket.org/vilos/peacerun'
env.sites_dirname = ''
env.requirements = 'requirements/project.txt'

@task
def live():
    #env.hosts = ['peacerun']
    env.hosts = ['newpr']

live()

from django.db.models.query import EmptyQuerySet
from django.conf import settings
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.reverse import reverse
from serializers import ErrorSerializer
from post_office import mail, PRIORITY
from conf import APP_ERRORS_SEND_EMAILS


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'team-members': reverse('api-team-list', request=request, format=format),
        'reports': reverse('api-report-list', request=request, format=format),
        'errors' : reverse('api-error-create', request=request, format=format)
    })


message = """
{{ description }}

-----------------

{{ logfile.read }}
"""

class ProcessError(generics.ListCreateAPIView):
    serializer_class = ErrorSerializer


    def get_queryset(self):
        return EmptyQuerySet()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)

        if serializer.is_valid():
            ctx = serializer.object
            to = APP_ERRORS_SEND_EMAILS
            subject = "Runporter error at {{ timestamp }}"

            mail.send( to, settings.SERVER_EMAIL, subject=subject,
                message=message, context=ctx, priority=PRIORITY.now
            )

            data = {'status' : 'OK' }
            return Response(data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from reports.api import ReportList, ReportDetail, ReportImageList, ReportImageDetail
from team_members.api import TeamList, TeamMemberDetail
from views import api_root, ProcessError



urlpatterns = patterns('',
    url(r'^$', api_root),
    url(r'^reports/$', ReportList.as_view(), name='api-report-list'),
    url(r'^reports/(?P<pk>[0-9]+)/$', ReportDetail.as_view(), name='api-report-detail'),
    url(r'^reports/(?P<pk>[0-9]+)/images/$', ReportImageList.as_view(), name='api-report-images'),
    url(r'^reports/(?P<pk>[0-9]+)/(?P<team>[a-zA-Z])/images/$', ReportImageList.as_view(), name='api-report-team-images'),
    url(r'^reports/(?P<report_id>[0-9]+)/images/(?P<position>[0-9]+)/$', ReportImageDetail.as_view(), name='api-report-image'),
    url(r'^reports/(?P<report_id>[0-9]+)/(?P<team>[a-zA-Z])/images/(?P<position>[0-9]+)/$', ReportImageDetail.as_view(), name='api-report-team-image'),

    url(r'^team-members/$', TeamList.as_view(), name='api-team-list'),
    url(r'^team-members/(?P<pk>[0-9]+)/$', TeamMemberDetail.as_view(), name='api-team-member-detail'),

    url(r'^errors/$', ProcessError.as_view(), name='api-error-create'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
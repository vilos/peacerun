from django.conf import settings

admin_emails = [a[1] for a in settings.ADMINS]
APP_ERRORS_SEND_EMAILS = getattr(settings, 'APP_ERRORS_SEND_EMAILS', admin_emails)
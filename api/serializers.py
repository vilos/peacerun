from rest_framework import serializers



class ErrorSerializer(serializers.Serializer):
    description = serializers.CharField(max_length=256)
    logfile = serializers.FileField(max_length=256)
    timestamp = serializers.CharField(max_length=100, required=False)

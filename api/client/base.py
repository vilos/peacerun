# coding: utf8
import sys
import getpass
import requests
from pprint import pprint
from requests.auth import HTTPBasicAuth


host = 'http://localhost:8000'
#host = 'http://peacerun.madalbalstudio.com'


def get_auth(path='../.auth'):
    auth = HTTPBasicAuth('','')
    saved = None
    try:
        saved = open(path).read()
    except IOError:
        pass

    if saved:
        user, pwd = saved.strip().split(':')
        user, pwd = user.strip(), pwd.strip()
    else:
        user = raw_input('User:')
        pwd = getpass.getpass()
        with open(path,'wb') as fp:
            fp.write(':'.join([user,pwd]))

    auth.username = user
    auth.password = pwd
    return auth


def resp(request):
    r = request
    if r.status_code >= 500:
        print 'Error'
        f = open('error.html', 'wb')
        print >>f, r.content
    else:
        print r.status_code, ' ',
        try:
            jo = r.json()
        except ValueError:
            if r.content:
                print r.content
            elif r.text:
                print r.text
        else:
            if jo:
                pprint(jo)

def get_url(path):
    return ''.join([host,path])

def main(**kw):
    if len(sys.argv) <= 1:
        print """
Usage:  client.py  method [arg1,2,3]
        """
    else:
        method = sys.argv[1]
        kw.update(locals())
        m = kw.get(method, None)
        if m:
            args = sys.argv[2:]
            m(*args)
        else:
            print "Method {0} not implemented.".format(method)

def get():
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/errors/')
    r = requests.get(url, headers=headers)
    resp(r)

def post():
    data = \
        {
            "description": u"nič tu nefunguje".encode('utf-8'),
            "timestamp": "2013-05-06 23:30",
         }
    files = {'logfile': ('test.log', open('test.log', 'rb'))}

    #headers = {'Content-Type' : 'application/json' }
    url = get_url('/api/errors/')
    r = requests.post(url, data=data, files=files, auth=get_auth())
    resp(r)


if __name__=='__main__':
    kw = locals()
    main(**kw)
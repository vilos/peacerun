# coding: utf8
import requests
import json

from peacerun.api.client import resp, get_auth, main

host = 'http://localhost:8000'
#host = 'http://peacerun.madalbalstudio.com'


def get_url(path):
    return ''.join([host,path])



def get():
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/team-members/')
    r = requests.get(url, headers=headers)
    resp(r)

def post():
    data = \
        {
            "name": "Jirka",
            "surname": u"Nespomalil",
            "country": "cz",
         }
#        {
#            "name": "Adam",
#            "surname": u"Rýchly",
#            "country": "sk",
#        },
    headers = {'Content-Type' : 'application/json' }
    url = get_url('/api/team-members/')
    r = requests.post(url, data=json.dumps(data), headers=headers, auth=get_auth())
    resp(r)

if __name__=='__main__':
    main()
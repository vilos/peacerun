import requests
import json
from base import resp, get_auth, main


host = 'http://localhost:8000'
#host = 'http://peacerun.madalbalstudio.com'


def get_url(path):
    return ''.join([host,path])


def send_report():
    print 'Creating report'
    data = {
            "country": "au",
            "date": "2013-08-10",
            "start_location": "Sydney, NSW",
            "end_location": "Cronulla, NSW",
            "headline": "test run",
            "distance": "199.000",
            "distance_units": "km",
            "team": "-",
            "runners": [110,8,160]
        }
    headers = {'Content-Type' : 'application/json' }
    url = get_url('/api/reports/')
    r = requests.post(url, data=json.dumps(data), headers=headers, auth=get_auth())
    resp(r)

def get_report(report_id):
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/reports/{0}/'.format(report_id))
    r = requests.get(url, headers=headers)
    resp(r)

def delete_report(report_id):
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/reports/{0}/'.format(report_id))
    r = requests.delete(url, headers=headers, auth=get_auth())
    resp(r)


def publish(report_id):
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/reports/{0}/'.format(report_id))
    data = {
        'published' : True
    }
    r = requests.patch(url, data=data, headers=headers, auth=get_auth())
    resp(r)

def get_images(report_id):
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/reports/{0}/images/'.format(report_id))
    r = requests.get(url, headers=headers)
    resp(r)


def get_report_image(report_id, position):
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/reports/{0}/images/{1}'.format(report_id, position))
    r = requests.get(url, headers=headers)
    resp(r)

def send_image(report_id):
    url = get_url('/api/reports/{0}/images/'.format(report_id))
    #headers = {'Content-Type' : 'application/json' }
    #headers = {'Content-Type' :'multipart/form-data'}


    files = {'image': ('test.jpg', open('test.jpg', 'rb'))}
    data = {
        'description' : 'description only',
        'description_local' : 'local',
        'show_in_report' : True,
        'tags' : ''
    }
    # json
    #r = requests.post(url, data=json.dumps(data), headers=headers, auth=get_auth())
    # multipart
    #r = requests.post(url, data=data, files=files, auth=get_auth()) #headers=headers,
    r = requests.post(url, data=data, files=files, auth=get_auth()) #headers=headers,
    resp(r)

def update_report_image(report_id, position):
    headers = {'Accept' : 'application/json'}
    url = get_url('/api/reports/{0}/images/{1}/'.format(report_id,position))
    data = {
        'description_local' : 'local patch',
    }
    files = {'image': ('test.jpg', open('test.jpg', 'rb'))}
    r = requests.patch(url, data=data, files=files, headers=headers, auth=get_auth())
    resp(r)



if __name__=='__main__':
    kw = locals()
    main(**kw)
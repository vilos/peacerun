from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _
from mezzanine.core.admin import StackedDynamicInlineAdmin
from countries.models import Country
from countries.utils import current_country_id, set_country_id
from models import Report, ReportImage




class ReportImageInline(StackedDynamicInlineAdmin):
    model = ReportImage


class ReportAdminForm(ModelForm):
    class Meta:
        model = Report

    def clean(self):
        # country field is required but hidden
        if not 'country' in self.cleaned_data:
            raise ValidationError(_('Choose a country in the dropdown above.'))

        self._validate_unique = True
        return self.cleaned_data

class ReportAdmin(admin.ModelAdmin):
    form = ReportAdminForm
    list_display = ('country', 'date', 'start_location', 'end_location', 'published', 'created_by', 'last_modified')
    list_editable = ('published',)
    date_hierarchy = 'date'
    inlines = (
               ReportImageInline,
               )

    fields = (
        'published',
        'date', 'start_location', 'end_location',
        'headline', 'headline_local',
        'distance',
        'distance_units',
        'image', 'image_crop_x', 'image_crop_y', 'image_crop_w', 'image_crop_h',
        'runners', 'photographers', 'authors',
        'additional_runners',
        'country',
    )
    filter_horizontal = ('runners', 'photographers', 'authors')

    def queryset(self, request):
        return Report.objects.filter(country_id=current_country_id())

    def get_object(self, request, object_id):
        """
        """
        if 'country_id' in request.GET:
            set_country_id(request)
        return super(ReportAdmin, self).get_object(request, object_id)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "country":
            country_id = current_country_id(request)
            # set default value
            kwargs['initial'] = country_id
            # limit options
            kwargs["queryset"] = Country.objects.filter(pk=country_id)

        return super(ReportAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


    def save_model(self, request, obj, form, change):
        if getattr(obj, 'created_by_id', None) is None:
            obj.created_by = request.user
        if not obj.country_id and getattr(request, 'country_id', None):
            obj.country_id = request.country_id
        obj.save()

admin.site.register(Report, ReportAdmin)
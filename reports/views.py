import datetime, logging
from django.conf import settings
from django.http import HttpResponseForbidden, Http404
from django.views.generic import ListView, DetailView, RedirectView
from django.utils.dateparse import parse_date
from models import Report, ReportImage
from forms import ReportForm, DATEPICKER_SEPARATOR
from reports.utils import all_thumbnails_exist

log = logging.getLogger('report')


class ReportList(ListView):
    """
    """
    paginate_by = 25
    
    model = Report

    def get_queryset(self):
        qs = Report.objects.all()
        country_id = self.kwargs.get('country_id', None)
        if country_id:
            qs = qs.filter(country_id=country_id)
        date_from, date_to = None, None
        date_range = self.kwargs.get('date_range', None)
        if date_range:
            dr = date_range.split(DATEPICKER_SEPARATOR)
            if len(dr) == 1:
                date_from = parse_date(dr[0])
            else:
                date_from, date_to = map(parse_date, dr)
                    
        if date_from:
            qs = qs.filter(date__gte=date_from)
            
        if date_to:
            qs = qs.filter(date__lte=date_to)            
        return qs

    def get_context_data(self, **kw):
        ctx = super(ReportList, self).get_context_data(**kw)
        
        # split reports by years
        # we assume the reports come sorted by descending date
        years = []
        yearIndex = -1
        for r in ctx['report_list']:
            year = r.date.year
            if yearIndex == -1 or years[yearIndex][0] != year:
                yearIndex += 1
                years.append( (year, []) )
            years[yearIndex][1].append(r)
        ctx['years'] = years
        if 'form' in self.kwargs:
            ctx['form'] = self.kwargs['form']
        else:
            ctx['form'] = ReportForm() 
        return ctx
    
    def post(self, request, **kwargs):
        data = request.POST.dict()
        self.kwargs['country_id'] = data.get('country_id', None)
        self.kwargs['date_range'] = data.get('date_range', None)
        #kwargs['date_to'] = data.get('date_to', None)
        self.kwargs['form'] = ReportForm(data)
        return self.get(request, **kwargs)


class ReportYear(ReportList):
    """
    """

    def get_queryset(self):
        return Report.objects.by_country(country_id=self.country_id, date__year=self.year)

    def get(self, request, *args, **kwargs):
        self.country_id = kwargs.pop('country_id')
        self.year = kwargs.pop('year')
        return super(ReportList, self).get(request, *args, **kwargs)


class ReportDetail(DetailView):
    model = Report
    context_object_name = 'report'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        report = self.object
        if not report.published:
            if self.request.user.is_anonymous():
                return HttpResponseForbidden()
        context = self.get_context_data(object=self.object)
        all_exist, not_ready_count = all_thumbnails_exist(context['report_items'])
        if not all_exist:
            self.template_name = 'reports/not_ready.html'
            context['not_ready_count'] = not_ready_count
            return self.render_to_response(context)
        return self.render_to_response(context)
    
    
    def get_context_data(self, **kwargs):
        ctx = super(ReportDetail, self).get_context_data(**kwargs)
        report = ctx.get('report', None)
        ctx['editable_obj'] = report
        ctx['report_items'] = ReportImage.objects.reported(report=report)
        ctx['runners'] = report.runners.distinct().select_related()
        ctx['authors'] = report.authors.distinct()
        ctx['photographers'] = report.photographers.distinct()
        ctx['embedded_video_width'] = settings.REPORT_VIDEO_SIZE[0]
        ctx['embedded_video_height'] = settings.REPORT_VIDEO_SIZE[1]
        ctx['additional_runners'] = ', '.join([r.strip(', ') for r in report.additional_runners.splitlines()])
        
        return ctx


class ReportDaily(RedirectView):

    def get_redirect_url(self, **kwargs):
        year = int(self.kwargs.pop('year'))
        monthday = self.kwargs.pop('month_day')
        month = int(monthday[:2])
        day = int(monthday[2:4])
        self.kwargs['date'] = datetime.date(year, month, day)
        reports = Report.objects.filter(**self.kwargs)
        if reports:
            report = reports[0]
            return report.get_absolute_url()
        raise Http404


class ReportGallery(DetailView):
    template_name = "reports/report_gallery.html"
    model = Report
    context_object_name = 'report'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        report = self.object
        if not report.published:
            if self.request.user.is_anonymous():
                return HttpResponseForbidden()
        context = self.get_context_data(object=self.object)
        all_exist, not_ready_count = all_thumbnails_exist(context['report_items'])
        if not all_exist:
            self.template_name = 'reports/not_ready.html'
            context['not_ready_count'] = not_ready_count
            return self.render_to_response(context)
        return self.render_to_response(context)
    
    
    def get_context_data(self, **kwargs):
        ctx = super(ReportGallery, self).get_context_data(**kwargs)
        report = ctx.get('report', None)
        ctx['report_items'] = ReportImage.objects.reported(report=report)
        
        ctx['source_sizes'] = settings.GALLERY_SCREEN_SIZES

        ctx['url_detail'] = report.get_absolute_url()
        
        return ctx

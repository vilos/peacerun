from django.conf.urls import patterns, url
from views import ReportDetail, ReportList, ReportYear, ReportDaily
from reports.views import ReportGallery

urlpatterns = patterns('',
    url(r'^news/$', ReportList.as_view(), name='global_news'),
    url(r'^(?P<country_id>[\w-]+)/news/$', ReportList.as_view(), name='report_list'),
    url(r'^(?P<country_id>[\w-]+)/news/(?P<year>\d{4})/$', ReportYear.as_view(), name='report_year'),
    url(r'^(?P<country_id>[\w-]+)/news/(?P<year>\d{4})/(?P<month_day>\d{4})/$', ReportDaily.as_view(), name='report_daily'),
    url(r'^(?P<country_id>[\w-]+)/news/(?P<year>\d{4})/(?P<month_day>\d{4})/(?P<pk>\d+)/$', ReportDetail.as_view(), name='report_detail'),
    url(r'^(?P<country_id>[\w-]+)/news/(?P<year>\d{4})/(?P<month_day>\d{4})/(?P<pk>\d+)/gallery/$', ReportGallery.as_view(), name='report_gallery'),
    url(r'^(?P<country_id>[\w-]+)/news/(?P<year>\d{4})/(?P<month_day>\d{4})/(?P<location_uuid>\w+)/$', ReportDaily.as_view(), name='report_daily_location'),
    #url(r'^(?P<country_id>[\w-]+)/news/$', ReportList.as_view(), name='report-list'),
    #url(r'^(?P<country_id>[\w-]+)/news/(?P<year>\d{4})/$', ReportYear.as_view(), name='report-year'),
)




DISTANCE_UNIT_CHOICES = (
    ('km','km'),
    ('mi','miles')
)

TEAM_CHOICES = (
    ('-', '-'),
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C'),
    ('D', 'D'),
)
NOTEAM = TEAM_CHOICES[0][0]
from rest_framework import permissions
from countries.models import CountryPermission
from reports.models import ReportImage


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet
        return obj.owner == request.user



class CountryEnabledOrReadOnly(permissions.BasePermission):
    """
    Check country enabled permission to edit.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True
        country_id = None
        if hasattr(obj, 'country'):
            country_id = obj.country.id
        elif hasattr(obj, 'report'):
            country_id = obj.report.country.id
        return CountryPermission.objects.check(request.user, country_id)

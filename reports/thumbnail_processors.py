



def crop_rect(image, crop_rect=False, **kwargs):
    """
    Crops the image if crop_rect is provided as 4-tuple (x, y, w, h) with values from [0,100]
    """
    
    if crop_rect:
        pixel_x = int(crop_rect[0] * image.size[0] / 100)
        pixel_y = int(crop_rect[1] * image.size[1] / 100)
        pixel_w = int(crop_rect[2] * image.size[0] / 100)
        pixel_h = int(crop_rect[3] * image.size[1] / 100)
        pixel_w = min(pixel_w, image.size[0] - pixel_x)
        pixel_h = min(pixel_h, image.size[1] - pixel_y)
        box = (pixel_x, pixel_y, pixel_x + pixel_w, pixel_y + pixel_h)
        image = image.crop(box)
    
    return image
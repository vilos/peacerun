from mezzanine import template
from ..models import Report
from settings import REPORT_THUMBNAIL_SIZE, LOCAL_NEWS_DISPLAY_COUNT,\
    GLOBAL_NEWS_DISPLAY_COUNT
from easy_thumbnails.files import get_thumbnailer
from django.utils import dateformat
from django.core.urlresolvers import reverse
from countries.utils import current_country_id
import string
import settings
from django.utils.safestring import mark_safe
import markdown
import markdown_newtab
from django.utils.encoding import force_text


register = template.Library()


@register.inclusion_tag("reports/side_menu.html", takes_context=True)
def country_reports(context):
    reports = list(Report.objects.by_country()[:LOCAL_NEWS_DISPLAY_COUNT])
    if len(reports) > 0:
        year = dateformat.DateFormat(reports[0].date).Y()
        context['menu_title'] = "NEWS " + str(year)
        # only select reports in the same year as the first one
        new_reports = []
        for r in reports:
            if dateformat.DateFormat(r.date).Y() == year:
                new_reports.append(r)
        reports = new_reports
    else:
        context['menu_title'] = "NEWS"
    context['more_news_url'] = reverse('report_list', args=(current_country_id(),))
    context['reports'] = reports
    context['report_thumb_width'] = REPORT_THUMBNAIL_SIZE[0]
    context['report_thumb_height'] = REPORT_THUMBNAIL_SIZE[1]
    return context


@register.inclusion_tag("reports/side_menu.html", takes_context=True)
def global_reports(context):
    context['menu_title'] = "GLOBAL NEWS"
    context['more_news_url'] = reverse('global_news')
    context['reports'] = Report.objects.globaly()[:GLOBAL_NEWS_DISPLAY_COUNT]
    context['report_thumb_width'] = REPORT_THUMBNAIL_SIZE[0]
    context['report_thumb_height'] = REPORT_THUMBNAIL_SIZE[1]
    return context


@register.inclusion_tag("parts/side_menu_global_news_changer.html", takes_context=True)
def global_reports_changer(context):
    context['menu_title'] = "GLOBAL NEWS"
    context['more_news_url'] = reverse('global_news')
    context['reports'] = Report.objects.globaly()
    context['report_thumb_width'] = REPORT_THUMBNAIL_SIZE[0]
    context['report_thumb_height'] = REPORT_THUMBNAIL_SIZE[1]
    return context


@register.simple_tag
def report_thumb_url(report):
    if not report.image:
        return "/static/images/null_report_thumb.png"
    thumbnailer = get_thumbnailer(report.image.path)
    thumb = thumbnailer.get_thumbnail(report.get_image_thumb_options(), generate=False)
    if thumb:
        return thumb.url
    else:
        return "/static/images/null_report_thumb.png"


@register.simple_tag
def report_item_video_html(video_url):
    if string.find(video_url, "youtube.com") != -1:
        watchpos = string.find(video_url, "watch?v=")
        if watchpos == -1:
            return ""
        code = video_url[watchpos+8:]
        html = '<iframe class="embedded-video" src="https://www.youtube.com/embed/' + code + '?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>'
        return html
    if string.find(video_url, "vimeo.com") != -1:
        slashpos = string.find(video_url, "/", string.find(video_url, "vimeo.com"))
        if slashpos == -1:
            return ""
        code = video_url[slashpos+1:]
        html = '<iframe class="embedded-video" src="https://player.vimeo.com/video/' + code + '?color=ced73c&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
        return html
    return ""


@register.filter(is_safe=True)
def report_desc_markdown(value, arg=''):
    return mark_safe(markdown.markdown(
                    force_text(value),
                    [markdown_newtab.NewTabExtension()], 
                    safe_mode=True, 
                    enable_attributes=False))
    

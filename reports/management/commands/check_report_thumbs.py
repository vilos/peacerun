from easy_thumbnails.files import get_thumbnailer
import django_rq
from django.core.management.base import BaseCommand
from reports.models import Report
from reports.tasks import generate_report_thumbnail
from optparse import make_option


class Command(BaseCommand):
    help = "Check if main report thumbnail exists or recreates it"
    
    option_list = BaseCommand.option_list + (
        make_option('-c', '--country',
            dest='country_id',
            help='filter by country code'),
    )

    def handle(self, *args, **options):
        qs = Report.objects.all().order_by('-created')
        country_id = options.get('country_id', None)
        if country_id:
            print("Filtering by country_id = {}".format(country_id))
            qs = qs.filter(country_id=country_id)            

        for r in qs:
            if r.image:
                path = r.image.path
                options = r.get_image_thumb_options()
                thumbnailer = get_thumbnailer(path)
                if thumbnailer.source_storage.exists(path):
                    if not thumbnailer.get_existing_thumbnail(options):
                        django_rq.enqueue(generate_report_thumbnail, path, options)
                else:
                    print 'missing thumb source:', path
            else:
                # this will invoke async_report_thumbnail()
                r.save()

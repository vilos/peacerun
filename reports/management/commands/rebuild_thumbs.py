import os
from optparse import make_option
from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand

# extended storage with move method
from filebrowser_safe.views import storage_class 

import django_rq

from reports.models import ReportImage
from reports.utils import image_path
from reports.tasks import delete_report_thumbnails


class Command(BaseCommand):
    args = '<country_id country_id ...>'
    help = 'Rebuild thumbnails for given countries with incorrect path'    
    option_list = BaseCommand.option_list + (
        make_option('--all',
            action='store_true',
            dest='all',
            default=False,
            help='include thumbnails with correct path'),
        )    
                
    def handle(self, *args, **options):
        queue = django_rq.get_queue('high')
        storage = storage_class()
        
        qs = ReportImage.objects.all().order_by('-created')
        if args:            
            qs = qs.filter(report__country_id__in=args)
        
        for ri in qs:
            correct = True
            if ri.image:
                path = ri.image.path
                
                # the following should correspond with serializers.save_image()           
                _, ext = os.path.splitext(path)
                correct_path = image_path(ri, ext)
                
                if path != correct_path:
                    correct = False
                    # move files
                    self.stdout.write('moving {} to {}'.format(path, correct_path))
                    
                    if storage.exists(correct_path):
                        # don't overwrite existing file
                        storage.move(path, correct_path)
                    else:
                        # save dummy file to create intermediate directories
                        # that do not exist
                        storage.save(correct_path, ContentFile(''))
                        # overwrite dummy file                     
                        storage.move(path, correct_path, allow_overwrite=True)
                    
                    
            if not correct or options['all']:
                # delete old thumbnails
                queue.enqueue(delete_report_thumbnails, path=path)
                                    
                # set correct path 
                ri.image.path = correct_path
                # enqueues creation of correct thumbnails
                ri.save()

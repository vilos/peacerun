import os
from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import connections
from optparse import make_option
from reports.models import ReportImage
from tabulate import tabulate


sql = """
select ri.id, ri.report_id, coalesce(ri.image,'') image, ri.position from reports_reportimage ri where ri.id = %s;
"""

def examine(relpath, dirpath):
    result = '-'
    path = ''
    if relpath:
        path = os.path.join(dirpath, relpath)
        if os.path.exists(path):
            result = os.path.getsize(path)
        else:
            result = 'X'    
    return result, path
    
class Command(BaseCommand):
    args = '<other project name>'
    help = "compare report images with another project"

    option_list = BaseCommand.option_list + (
        make_option('-r', '--report',
            type='int',
            dest='report_id',
            help='filter by report id'),
        make_option('-c', '--country',
            dest='country_id',
            help='filter by country code'),
        )

    def handle(self, *args, **options):
        if not args:
            self.stderr.write('Please provide other project name to compare with.')
            return
        old_project = settings.PROJECT_DIRNAME
        project = args[0]
        media_root = settings.MEDIA_ROOT
        new_media_root = settings.MEDIA_ROOT.replace(old_project, project)
        cursor = connections[project].cursor()
        qs = ReportImage.objects.all().order_by('report', 'position')
        
        report_id = options.get('report_id', None)
        if report_id:
            print("Filtering by report_id = {}".format(report_id))
            qs = qs.filter(report_id=report_id)
        
        country_id = options.get('country_id', None)
        if country_id:
            print("Filtering by country_id = {}".format(country_id))
            qs = qs.filter(report__country_id=country_id)            

        headers = ['id', 'report_id', 'position', old_project, project]
        table = []
        for ri in qs:
            self.stdout.write("Report {} - position {}".format(ri.report_id, ri.position))
            oldimage = getattr(ri.image, 'path', u'')
            row = [ri.pk, ri.report_id, ri.position]
            
            old, oldpath = examine(oldimage, media_root)

            
            cursor.execute(sql, [ri.pk])
            found = cursor.fetchone()
            if found:
                pk, report_id, image, position = found
                new, newpath = examine(image, new_media_root)
            else:
                new = '#'

            if (old == '-' and new == '-') or \
                (isinstance(old, (int, long)) and isinstance(new, (int, long)) \
                 and old == new):
                continue
            else:
                if isinstance(old, (int,long)):
                    row.append(oldpath)
                else: 
                    row.append(old)
                if isinstance(new, (int,long)):
                    row.append(newpath)
                else: 
                    row.append(new)
            table.append(row)
        print tabulate(table, headers=headers)
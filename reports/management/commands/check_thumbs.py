from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.alias import aliases
import django_rq
from django.core.management.base import BaseCommand
from reports.models import ReportImage
from reports.tasks import generate_missing_thumbnail


class Command(BaseCommand):
    help = "Check if all report image thumbnails exist or recreate them"

    def handle(self, *args, **options):
        for ri in ReportImage.objects.all().order_by('-created'):
            if ri.image:
                path = ri.image.path                
                all_options = aliases.all(path, include_global=True)
                thumbnailer = get_thumbnailer(path)
                if thumbnailer.source_storage.exists(path):
                    if all_options:
                        for options in all_options.values():
                            if not thumbnailer.get_existing_thumbnail(options):
                                print 'missing: ', path, options                        
                                django_rq.enqueue(generate_missing_thumbnail, path, options, high_resolution=False)
                else:
                    print 'missing thumb source:', path
                        
                    
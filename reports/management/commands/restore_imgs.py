import os, shutil
import hashlib
from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import connections
from optparse import make_option
from reports.models import ReportImage
from reports.tasks import delete_report_thumbnails
from reports.utils import image_path
from tabulate import tabulate

# extended storage with move method
#from filebrowser_safe.views import storage_class 
import django_rq


sql = """
select ri.id, ri.report_id, coalesce(ri.image,'') image, ri.position from reports_reportimage ri where ri.id = %s;
"""


def hashfile(path):
    return hashlib.sha256(open(path, 'rb').read()).digest()
    
    
def examine(relpath, dirpath):
    status = '-'
    path = ''
    size = 0
    if relpath:
        path = os.path.join(dirpath, relpath)
        if os.path.exists(path):
            status = 'Y'
            size = os.path.getsize(path)
        else:
            status = 'X'    
    return status, path, size

def get_queryset(**options):
    
    qs = ReportImage.objects.all().order_by('report', 'position')
    
    report_id = options.get('report_id', None)
    if report_id:
        print("Filtering by report_id = {}".format(report_id))
        qs = qs.filter(report_id=report_id)
    
    country_id = options.get('country_id', None)
    if country_id:
        print("Filtering by country_id = {}".format(country_id))
        qs = qs.filter(report__country_id=country_id)
    return qs


class Command(BaseCommand):
    args = '<other project name>'
    help = "restore report images from another project"

    option_list = BaseCommand.option_list + (
        make_option('-r', '--report',
            type='int',
            dest='report_id',
            help='filter by report id'),
        make_option('-c', '--country',
            dest='country_id',
            help='filter by country code'),
        make_option('-m', '--move',
            action='store_true',
            dest='move',
            help='move files from other project (b) to current (a)'),
        )
  

    def handle(self, *args, **options):
        if not args:
            self.stderr.write('Please provide other project name to restore from.')
            return
        project_a = settings.PROJECT_DIRNAME
        project_b = args[0]
        media_root_b = settings.MEDIA_ROOT.replace(project_a, project_b)        
        cursor = connections[project_b].cursor()

        move = options.get('move', False)
        
        qs = get_queryset(**options)
        
        if move:
            queue = django_rq.get_queue('high')
            #storage = storage_class()
        
        table = []
        headers = ['id', 'report_id', 'team', 'position', 'status_a', project_a, 'status_b', project_b]
        for ri in qs:
            #self.stdout.write("Report {} - {} - position {}".format(
            #                    ri.report_id, ri.pk, ri.position))
            row = [ri.pk, ri.report_id, ri.team, ri.position]
                
            image = getattr(ri.image, 'path', u'')            
            status_a, path_a, size_a = examine(image, settings.MEDIA_ROOT)
            
            cursor.execute(sql, [ri.pk])
            found = cursor.fetchone()
            if found:
                pk_b, report_id, image_b, position = found
                status_b, path_b, size_b = examine(image_b, media_root_b)
            else:
                status_b = '#'
                path_b = ''
    
            # no files assigned or equal size
            if (status_a == '-' and status_b == '-')\
                or (status_a == 'Y' and status_b == 'Y' and size_a == size_b):
                if (size_a == 0 and size_b == 0) or\
                   (hashfile(path_a) == hashfile(path_b)):
                    continue
                    
            row.append(status_a)
            row.append(path_a)
            row.append(status_b) 
            row.append(path_b)
            
            table.append(row)
            
            if path_b and os.path.exists(path_b):
                _, ext = os.path.splitext(path_b)
                
                if move:
                    # remove old thumbs
                    queue.enqueue(delete_report_thumbnails, path=image)
                
                correct_path = image_path(ri, ext)
                full_path = os.path.join(settings.MEDIA_ROOT, correct_path)
                
                # skip if old path exists
                if os.path.exists(full_path): 
                    self.stdout.write('path already exists {} '.format(path_a))
                else:
                    self.stdout.write('copy {} to {}'.format(path_b, full_path))
                    if move:
                        shutil.copy2(path_b, full_path)
                
                        # set correct path 
                        ri.image.path = correct_path
                        # enqueues creation of correct thumbnails
                        ri.save()
                                
        print tabulate(table, headers=headers)
        
        if not move:
            self.stdout.write("Nothing was copied.")
        else:
            self.stdout.write("All done.")        
            

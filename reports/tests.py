"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from datetime import date
from django.test import TestCase
from django.contrib.auth.models import User
from .serializers import ReportSerializer
from .conf import TEAM_CHOICES
from .models import Report, ReportImage
from .utils import image_path
from calendar import month

data =  {
    "title": "test1",
    "date": "2001-03-20",
    "start_location": "Praha",
    "end_location": "Brno",
    "country": "cz",
    "distance": "123.000",
    "distance_units": "mi",    
}
        
class SimpleTest(TestCase):

    def setUp(self):
        from countries.models import Country
        self.c = Country(pk = 'cz', iso3='CZE', name='Czech Republic', active=True)
        self.c.save()
        self.user = User.objects.create_user('vilos', 'vilos@vs.vs', '1234')
        

    def test_serializer(self):
        """
        """        

        data["is_partitioned"] =  True
        rs = ReportSerializer(data=data)
        valid = rs.is_valid()
        if not valid:
            print rs.errors
        self.assertEqual(valid, True)

        data['title'] = ''
        rs = ReportSerializer(data=data)        
        self.assertEqual(rs.is_valid(), True)

    def test_reportimage_name(self):
        rd = data.copy()
        rd['created_by'] = self.user
        rd['country'] = self.c
        rd['date'] = date(year=2001, month=3, day=20)
        report = Report.objects.create(**rd)       
        ri1 = ReportImage.objects.create(
            report=report,
            team=TEAM_CHOICES[0][0],
            position=1
        )
        riA = ReportImage.objects.create(
            report=report,
            team='A',
            position=1
        )        
        riB = ReportImage.objects.create(
            report=report,
            team='B',
            position=2
        )        
        
        self.assertEqual(image_path(ri1, '.jpg'), 'uploads/reports/cz/2001/0320/1-1.jpg')
        self.assertEqual(image_path(riA, '.png'), 'uploads/reports/cz/2001/0320/1-A-1.png')
        self.assertEqual(image_path(riB, '.txt'), 'uploads/reports/cz/2001/0320/1-B-2.txt')
        
        
        
        
        
        
        
        
        
        
        
        
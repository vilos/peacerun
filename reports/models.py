import os
from collections import Counter
from datetime import timedelta
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.utils.dateformat import format
from django.utils.translation import ugettext_lazy as _
import django_rq
from easy_thumbnails.signals import thumbnail_missed
from mezzanine.core.fields import FileField
from mezzanine.utils.models import upload_to
from mezzanine.utils.urls import admin_url
from geoposition.fields import GeopositionField
from countries.models import CountryRelatedRequired
from countries.utils import current_country_id
from conf import DISTANCE_UNIT_CHOICES, TEAM_CHOICES, NOTEAM
from utils import make_title, make_stamp
from reports.utils import make_location_uuid, get_folder, image_subpath,\
    make_fromto
from tasks import generate_report_thumbnails, generate_missing_thumbnail, delete_report_thumbnails
from reports.tasks import generate_report_thumbnail
from settings import REPORT_THUMBNAIL_SIZE, GLOBAL_NEWS_DISPLAY_COUNT,\
    GLOBAL_NEWS_CHANGER_COUNT


class ReportManager(models.Manager):

    def published(self, **kwargs):
        return self.filter(published=True, **kwargs)

    def by_country(self, country_id=None, **kwargs):
        if not country_id:
            country_id = current_country_id()
        return self.published(country_id=country_id, **kwargs)

    def globaly(self, **kwargs):
        """ latest reports, one from each country """
        qs = self.published().select_related()
        ret = []
        countries = Counter()
        i = 0
        for r in qs:
            if countries[r.country_id] > 0:
                continue
            countries[r.country_id] += 1
            ret.append(r)
            i += 1
            if i >= max(GLOBAL_NEWS_CHANGER_COUNT, GLOBAL_NEWS_DISPLAY_COUNT):
                break
        return ret


class Report(CountryRelatedRequired):
    date = models.DateField(_('Date'))

    start_location = models.CharField(_('Start location'), max_length=255)
    end_location = models.CharField(_('End location'), max_length=255)
    start_position = GeopositionField(_('Start position'), blank=True, null=True)
    end_position = GeopositionField(_('End position'), blank=True, null=True)
    location_uuid = models.CharField(_('Location based identifier'), max_length=32, default='--')

    headline = models.CharField(_('Headline'), max_length=50, blank=True, null=True,
            help_text="Headline for the report used in lists. Not more than 50 chars.")
    headline_local = models.CharField(_('Headline in local language'), max_length=50, blank=True, null=True,
            help_text="Report headline in local language. Not more than 50 chars.")
    image = FileField(_("Image"), max_length=255, format="Image",
            upload_to=upload_to("reports.ReportImage.file", "reports"), blank=True, null=True)
    image_crop_x = models.FloatField(_("Image Crop X [0,1]"), default=0)
    image_crop_y = models.FloatField(_("Image Crop Y [0,1]"), default=0)
    image_crop_w = models.FloatField(_("Image Crop Width [0,1]"), default=1)
    image_crop_h = models.FloatField(_("Image Crop Height [0,1]"), default=1)

    distance = models.DecimalField(_('Distance'), null=True, max_digits=9, decimal_places=3)
    distance_units = models.CharField(max_length='2', choices=DISTANCE_UNIT_CHOICES, default=DISTANCE_UNIT_CHOICES[0][0])

    team = models.CharField(_('Team'), max_length='1', choices=TEAM_CHOICES, default=TEAM_CHOICES[0][0],
            help_text="Team of the last report update.")
    runners = models.ManyToManyField('team_members.TeamMember', null=True, related_name='reports_run')
    additional_runners = models.TextField(_("Additional Runners"), blank=True, null=True)
    photographers = models.ManyToManyField('team_members.TeamMember', null=True, related_name='reports_pictured')
    authors = models.ManyToManyField('team_members.TeamMember', null=True, related_name='reports_written')

    created_by = models.ForeignKey('auth.User', related_name='reports')
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    last_modified = models.DateTimeField(_('Last modified'), auto_now=True)
    published = models.BooleanField(_('Published'), default=False)

    objects = ReportManager()

    class Meta(object):
        get_latest_by = ('date')
        unique_together = (("country", "date", "team", "location_uuid"),)
        ordering = ('-date',)

    @property
    def title(self):
        return make_title(self.country.name, self.date, self.start_location, self.end_location)

    @property
    def stamp(self):
        return make_stamp(self.country.name, self.date)

    @property
    def fromto(self):
        return make_fromto(self.start_location, self.end_location)

    def __unicode__(self):
        return self.title

    def save(self, **kwargs):
        self.location_uuid = make_location_uuid(self.start_location, self.end_location)
        super(Report, self).save(**kwargs)

    @property
    def year(self):
        return format(self.date, "Y")

    @property
    def month_day(self):
        return format(self.date, "md")

    @models.permalink
    def get_absolute_url(self):
        return ('report_detail', (), {
            'country_id': self.country_id,
            'year': self.year,
            'month_day': self.month_day,
            'pk': self.pk
        })

    @models.permalink
    def gallery_url(self):
        return ('report_gallery', (), {
            'country_id': self.country_id,
            'year': self.year,
            'month_day': self.month_day,
            'pk': self.pk
        })

    def get_admin_url(self):
        url = admin_url(self, "change", self.id)
        url = '{}?country_id={}'.format(url, self.country_id)
        return url

    def previous(self):
        """ reports from previous days """
        reports = list(Report.objects.published().filter(date=self.date-timedelta(days=1)))
        reports, changed = self.pull_same_country_up(reports)
        if not changed:
            # there is no report from our country, let's find some of a date further away
            to_add = Report.objects.published(country=self.country, date__lt=self.date, date__year=self.date.year).order_by('-date')[:1]
            if to_add:
                reports.insert(0, to_add[0])
        return reports

    def next(self):
        """ reports from following days """
        reports = list(Report.objects.published().filter(date=self.date+timedelta(days=1)))
        reports, changed = self.pull_same_country_up(reports)
        if not changed:
            # there is no report from our country, let's find some of a date further away
            to_add = Report.objects.published(country=self.country, date__gt=self.date, date__year=self.date.year).order_by('date')[:1]
            if to_add:
                reports.insert(0, to_add[0])
        return reports

    def sameday(self):
        """ reports from the same day; self will be at the top of the list """
        reports = list(Report.objects.published().filter(date=self.date))
        reports, _ = self.pull_same_country_up(reports)

        # now pull myself up
        if self in reports:
            reports.remove(self)
        reports.insert(0, self)

        return reports

    def pull_same_country_up(self, reports):
        """ pulls reports of the same country as the current report up in the list """
        """ returns changed list and boolean determining whether there was any change """
        changed = False
        results = []

        # first our country
        results += [r for r in reports if r.country == self.country]
        if results:
            changed = True

        # now the rest
        results += [r for r in reports if r.country != self.country]

        return results, changed


    def merge_members(self, runners, photographers, authors, additional_runners=''):
        self.runners.add(*runners)
        self.photographers.add(*photographers)
        self.authors.add(*authors)
        self.additional_runners = self.additional_runners + additional_runners

    def clean(self):
        """ check if the report is unique with regard to report team """
        location_uuid = make_location_uuid(self.start_location, self.end_location)
        old_reports = Report.objects.exclude(pk=self.pk).filter(country=self.country, date=self.date, location_uuid=location_uuid)
        if self.team == NOTEAM:
            self.team = 'A'

        for old in old_reports:
            if old.team == NOTEAM:
                old.team = 'A'

            error = {
                'id' : [str(old.pk)],
                NON_FIELD_ERRORS : ["Report with this Country, Date, Location already exists."]
            }
            if old.team != self.team:
                error['team'] = [old.team]
            raise ValidationError(error)

    def get_image_thumb_options(self):
        # if crop bounds are null, assume it means full crop
        if not self.image_crop_x:
            self.image_crop_x = 0
        if not self.image_crop_y:
            self.image_crop_y = 0
        if not self.image_crop_w:
            self.image_crop_w = 1
        if not self.image_crop_h:
            self.image_crop_h = 1

        options = { 'size': REPORT_THUMBNAIL_SIZE,
                    # this option will go to our custom processor 'crop_rect'
                    'crop_rect': (self.image_crop_x*100, self.image_crop_y*100, self.image_crop_w*100, self.image_crop_h*100),
                    'crop': True,
                    'upscale': True }
        return options

    def ensure_has_image(self):
        if (self.image or self.images.count() == 0):
            return

        # grab the first image as the report image
        for img in self.images.all():
            if img.image:
                self.image = img.image
                break

        # we'll set the crop bounds to max even though aspect ratio may be off, but the image will get
        # cropped down to match the right aspect ratio using default image processors in easy-thumbnails
        if self.image:
            self.image_crop_x = 0
            self.image_crop_y = 0
            self.image_crop_w = 1
            self.image_crop_h = 1
            self.save()



class ReportImageManager(models.Manager):

    def reported(self, **kwargs):
        return self.filter(show_in_report=True, **kwargs)

    def next_position(self, report, team):
        return self.filter(report=report, team=team).count() + 1


class ReportImage(models.Model):
    report = models.ForeignKey(Report, related_name= 'images')
    team = models.CharField(_('Team'), max_length='1', choices=TEAM_CHOICES, default=TEAM_CHOICES[0][0],
            help_text="Select the team in case of multiple teams running in the same day.")
    image = FileField(_("Image"), max_length=255, format="Image",
        upload_to=upload_to("reports.ReportImage.file", "reports"), blank=True, null=True)
    description = models.TextField(_("Description (english)"), blank=True, null=True)
    description_local = models.TextField(_("Description (local language)"), blank=True, null=True)    #markdown
    position = models.SmallIntegerField(_("Position"), default=0)
    show_in_report = models.BooleanField(_("Show in report"), default=True)
    tags = models.CharField(_("Tags"), max_length=255, blank=True, null=True,
            help_text=_("Please enter special location or person on the photo. Multiple tags can be separated by comma."))
    video_url = models.CharField(_("Video URL"), max_length=255, blank=True, null=True,
            help_text=_("URL of video to be embedded into the report."))

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    last_modified = models.DateTimeField(_('Last modified'), auto_now=True)

    objects = ReportImageManager()
    #tags = TaggableManager()

    class Meta(object):
        ordering = ('team','position',)

    def __unicode__(self):
        return u"({0})-{1}".format(self.report_id, self.position)

    def get_admin_url(self):
        return self.report.get_admin_url()

    #TODO:  move image file naming and moving here
#     def clean(self):
#         pass

    def save(self, *args, **kwargs):
        if self.position == 0:
            self.position = ReportImage.objects.next_position(self.report, self.team)

        super(ReportImage, self).save(*args, **kwargs)


@receiver(post_save, sender=ReportImage)
def async_thumbnails(sender, instance, **kwargs):
    if instance.image:
        queue = django_rq.get_queue('high')
        queue.enqueue(generate_report_thumbnails, path=instance.image.path)

@receiver(thumbnail_missed)
def async_missing_thumbnails(sender, options, high_resolution, **kwargs):
    django_rq.enqueue(generate_missing_thumbnail, sender.name, options, high_resolution)

@receiver(pre_delete, sender=ReportImage)
def async_delete_thumbnails(sender, instance, **kwargs):
    if instance.image:
        queue = django_rq.get_queue('high')
        queue.enqueue(delete_report_thumbnails, path=instance.image.path)

@receiver(post_save, sender=Report)
def async_report_thumbnail(sender, instance, **kwargs):
    instance.ensure_has_image()
    if instance.image:
        queue = django_rq.get_queue('high')
        queue.enqueue(generate_report_thumbnail, path=instance.image.path, options=instance.get_image_thumb_options())

@receiver(pre_delete, sender=Report)
def async_delete_report_thumbnail(sender, instance, **kwargs):
    if instance.image:
        queue = django_rq.get_queue('high')
        queue.enqueue(delete_report_thumbnails, path=instance.image.path)

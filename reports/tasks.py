from django_rq import job
from easy_thumbnails.alias import aliases
from easy_thumbnails.files import get_thumbnailer, generate_all_aliases
from easy_thumbnails.storage import thumbnail_default_storage
from easy_thumbnails.models import Source, Thumbnail


@job
def generate_report_thumbnails(path):
    """ generate all thumbnails specified in global settings for given image """
    generate_all_aliases(path, include_global=True)


@job
def generate_missing_thumbnail(path, options, high_resolution):
    """ generate thumbnail if source path exists """
    thumbnailer = get_thumbnailer(path)
    if thumbnailer.source_storage.exists(path):
        if high_resolution:
            options['HIGH_RESOLUTION'] = high_resolution
        thumbnailer.get_thumbnail(options, generate=True)
    else:
        #TODO: announce missing file
        print "generate_missing_thumbnail: missing source file: " + path
        pass


@job
# deprecated
def _delete_report_thumbnails(path):
    thumbnailer = get_thumbnailer(path)
    all_options = aliases.all(path, include_global=True)
    if all_options:
        for options in all_options.values():
            thumb = thumbnailer.get_existing_thumbnail(options)
            if thumb:
                thumb.storage.delete(thumb.name)


@job
def delete_report_thumbnails(path):
    storage = thumbnail_default_storage

    qs = Source.objects.filter(name__startswith=path)
    
    for source in qs:
        thumbs_to_delete = []
        for thumb in source.thumbnails.all():            
            if storage.exists(thumb.name):
                storage.delete(thumb.name)
            thumbs_to_delete.append(thumb.id)
        Thumbnail.objects.all().filter(id__in=thumbs_to_delete).delete() 
        
        
@job
def generate_report_thumbnail(path, options):
    """ generate report's main thumbnail displayed when listing reports """
    thumbnailer = get_thumbnailer(path)
    thumbnailer.get_thumbnail(options, generate=True)


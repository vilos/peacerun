from linkcheck import Linklist
from reports.models import ReportImage

class ReportImageLinklist(Linklist):
    model = ReportImage
    object_filter = {'report__published': True}
    markdown_fields = ['description', 'description_local']
        
linklists = {'Report Images': ReportImageLinklist}
from django import forms
from countries.models import Country
from models import Report


# class DistinctYearField(forms.ModelChoiceField):
#     def label_from_instance(self, obj):
#         if obj:
#             return obj.year

DATEPICKER_SEPARATOR = ' : '

class ReportForm(forms.Form):

    country_id = forms.ModelChoiceField(label="Country", queryset=Country.objects.active(), required=False)
    #year = DistinctYearField(queryset=Report.objects.published().dates('date', 'year'))
    #date_from = forms.DateField(label="Date from", required=False)
    #date_to = forms.DateField(label="Date to", required=False)
    date_range = forms.CharField(label="Date range", required=False, 
                                 widget=forms.TextInput(attrs={
                                    'data-range': 'true',
                                    'data-multiple-dates-separator': DATEPICKER_SEPARATOR,
                                    'data-language': 'en',
                                    'class': 'datepicker-here'
                                 }))
    
    def __init__(self, *args, **kwargs):
        super(ReportForm, self).__init__(*args, **kwargs)
        
        
        


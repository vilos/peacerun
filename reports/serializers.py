import os
from django.core.files.storage import default_storage
from django.utils.encoding import smart_unicode
from rest_framework import serializers
from countries.models import Country
from team_members.models import TeamMember
import filebrowser_safe.views    # default storage magic
from filebrowser_safe.base import FileObject
from models import Report, ReportImage
from utils import image_path
from reports.conf import NOTEAM



class ReportSerializer(serializers.ModelSerializer):

    country = serializers.PrimaryKeyRelatedField(many=False, queryset=Country.objects.active())
    runners = serializers.PrimaryKeyRelatedField(many=True, required=False, queryset=TeamMember.objects.all())
    photographers = serializers.PrimaryKeyRelatedField(many=True, required=False, queryset=TeamMember.objects.all())
    authors = serializers.PrimaryKeyRelatedField(many=True, required=False, queryset=TeamMember.objects.all())

    class Meta:
        model = Report
        fields = ('id', 'country', 'date',
                  'start_location', 'end_location', 'location_uuid',
                  'start_position', 'end_position',
                  'headline', 'headline_local', 'distance', 'distance_units', 'team',
                  'runners', 'photographers', 'authors', 'additional_runners',
                  'published'
                  )

        read_only_fields = ('id',)


class ImageObject(object):
    def __init__(self, data):
        self.name = ''
        if data:
            self.name = data.name
            

class ReportImageSerializer(serializers.ModelSerializer):

    report = serializers.PrimaryKeyRelatedField(many=False, queryset=Report.objects.all())
    image = serializers.ImageField(required=False, max_length=255)

    class Meta:
        model = ReportImage
        fields = ('report', 'image', 'team', 'description', 'description_local', 'show_in_report', 'tags', 'video_url')
        
    def save_image(self, instance, imagedata):
        _, ext = os.path.splitext(imagedata.name)
        path = image_path(instance, ext)
        exists = default_storage.exists(path)
        uploadedfile = default_storage.save(path, imagedata)
        # if file already exists, default_storage will save new file under a new name,
        # therefore we rename it to the original one
        if exists:
            default_storage.move(smart_unicode(uploadedfile), smart_unicode(path), allow_overwrite=True)
        instance.image = FileObject(path)

    def restore_object(self, attrs, instance=None):
        # pull out the image file
        image = attrs.pop('image', None)
        if instance:
            for attr in attrs:
                setattr(instance, attr, attrs[attr])
        else:
            report = attrs['report']
            team = attrs.get('team', NOTEAM)
            attrs['position'] = ReportImage.objects.next_position(report, team)
            instance = ReportImage(**attrs)
        if image:
            self.save_image(instance, image)

        return instance

import os
import shortuuid
import django_rq
from django.utils import dateformat
from filebrowser_safe.functions import get_directory
from easy_thumbnails.alias import aliases
from easy_thumbnails.files import get_thumbnailer
from countries.utils import current_country_id
from reports.tasks import generate_missing_thumbnail
from conf import TEAM_CHOICES


def get_folder(*args, **kwargs):

    prefix = 'reports'
    country_id = current_country_id()

    folder =  '/'.join([prefix, country_id])
    return folder


def image_subpath(report, team, position, ext):
    """ format YYYY/MMDD/{report_id}[-{team}]-{position}.{ext}"""
    md = dateformat.format(report.date, 'md')
    return u"{year}/{md}/{report_id}{team}-{position}{ext}".format(
                year = str(report.date.year),
                md=md, 
                report_id=str(report.pk),
                team = '-' + team if team != TEAM_CHOICES[0][0] else '',  
                position=str(position), 
                ext=ext.lower())


def image_path(reportimage, ext):
    subpath = image_subpath(reportimage.report, reportimage.team,
                            reportimage.position, ext)
    folder = 'reports/{0}'.format(reportimage.report.country.pk)
    return os.path.join(get_directory(), folder, subpath)    


def makeup_filename(report, team, position, ext):
    """ format : YYYY-MM-DD-{team}-{position}.jpg """

    dt = dateformat.format(report.date, 'Ymd')
    return u'{0}_{1}-{2}{3}'.format(dt, team, str(position), ext.lower())

def make_stamp(country, date):
    return u"{0} {1}".format(country, dateformat.format(date, "j F"))

def make_title(country, date, start_location, end_location, team=TEAM_CHOICES[0][0]):
    r = make_stamp(country, date)
    if start_location:
        r = u"{0}: {1}".format(r, start_location)
        if end_location and start_location != end_location:
            r = u"{0} - {1}".format(r, end_location)
    if team != TEAM_CHOICES[0][0]:
        r = u"{0}, Team {1}".format(r, team)
    return r

def make_fromto(start_location, end_location):
    r = ""
    if start_location:
        r = start_location
    if end_location and start_location != end_location:
        r = u"{0} - {1}".format(r, end_location)
    return r

def join_members(members):
    return ', '.join([' '.join([a.name, a.surname]) for a in members])

def join_members_country(members):
    return ', '.join([' '.join([a.name, a.surname, '({0})'.format(a.country.name)]) for a in members])


SHORT_UUID_LENGTH = 8

def uuidencode(name=None):
    return shortuuid.uuid(name=name)[:SHORT_UUID_LENGTH]

def make_location_uuid(start_location, end_location):
    locid = u''.join([start_location, end_location])
    locid = str(locid.encode('ascii', 'backslashreplace'))
    return uuidencode(locid)

def merge_team_reports(a, r):
    for ri in r.images.all():
        ri.report = a
        ri.save()
        
def all_thumbnails_exist(report_items):
    not_ready_count = 0
    for ri in report_items:
        image_not_ready = False
        if not ri.image:
            continue
        path = ri.image.path
        all_options = aliases.all(path, include_global=True)
        thumbnailer = get_thumbnailer(path)
        if thumbnailer.source_storage.exists(path) and all_options:
            for options in all_options.values():
                if not thumbnailer.get_existing_thumbnail(options):
                    image_not_ready = True
                    django_rq.enqueue(generate_missing_thumbnail, path, options, high_resolution=False)
        if image_not_ready:
            not_ready_count = not_ready_count + 1
    return not_ready_count==0, not_ready_count
        

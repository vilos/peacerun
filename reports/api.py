from django.core.exceptions import NON_FIELD_ERRORS, ObjectDoesNotExist
from django.http import Http404
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from models import Report, ReportImage
from serializers import ReportSerializer, ReportImageSerializer
from permissions import CountryEnabledOrReadOnly
from utils import make_location_uuid
from conf import TEAM_CHOICES
from reports.conf import NOTEAM


class ReportList(generics.ListCreateAPIView):
    """
    Concrete view for listing a queryset or creating a model instance.
    """

    model = Report
    serializer_class = ReportSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, CountryEnabledOrReadOnly)

    def get_queryset(self):
        return Report.objects.all()[:10]

    def create(self, request, *args, **kwargs):
        data=request.DATA.copy()
        if 'start_location' in data and 'end_location' in data:
            data['location_uuid'] = make_location_uuid(data['start_location'], data['end_location'])

        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            data = {'id' : self.object.pk}
            return Response(data, status=status.HTTP_201_CREATED)

        errors = serializer.errors

        if 'id' in errors:
            errors['id'] = int(errors['id'][0])

        if 'team' in errors:
            old = Report.objects.get(pk=errors['id'])
            self.merge_reports(old, **data)
            return Response(errors, status=status.HTTP_200_OK)

        if NON_FIELD_ERRORS in errors:
            msg = errors.pop(NON_FIELD_ERRORS)
            if isinstance(msg, (list,tuple)):
                msg = '\n'.join(msg)
            errors['message'] = msg
            return Response(errors, status=status.HTTP_409_CONFLICT)

        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

    def pre_save(self, obj):
        obj.created_by = self.request.user

    def merge_reports(self, old, runners=[], photographers=[], authors=[], **kw):
        # merge and update
        team = kw['team']
        old.merge_members(runners, photographers, authors, kw.get('additional_runners', ''))
        # in case that new team is A, incoming metadata will be stored in report
        if team == 'A':
            for k,v in kw.items():
                # skip fields we don't need to set
                if k in ('country', 'date', 'location_from', 'location_to'):
                    continue
                setattr(old, k, v)
        old.save()

class ReportDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Report
    serializer_class = ReportSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, CountryEnabledOrReadOnly)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        self.object = self.get_object()
        serializer = self.get_serializer(self.object, data=request.DATA,
                                         files=request.FILES, partial=partial)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_update= True)
            self.post_save(self.object, created=False)
            pk = self.object.pk
            data = {'id' : pk, 'message': 'Report id={0} updated.'.format(pk) }
            return Response(data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def pre_save(self, obj):
        user = self.request.user
        obj.created_by = user
        super(ReportDetail, self).pre_save(obj)

    def destroy(self, request, *args, **kwargs):
        obj = self.get_object()
        pk = obj.pk
        #if obj.published:
        #    data = {'id' : pk, 'message': "Can't delete published report." }
        #    return Response(data, status=status.HTTP_406_NOT_ACCEPTABLE)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ReportImageList(generics.ListCreateAPIView):
    model = ReportImage
    serializer_class = ReportImageSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, CountryEnabledOrReadOnly)

    def get_queryset(self):
        pk = self.kwargs['pk']
        team = self.kwargs.get('team', NOTEAM)
        try:
            report = Report.objects.get(pk=pk)
        except Report.DoesNotExist:
            raise Http404('No report with id {0}.'.format(pk))

        return report.images.filter(team=team)

    @method_decorator(never_cache)
    def list(self, request, *args, **kwargs):
        qs = self.get_queryset()
        count = qs.count()
        return Response({'images' : count})

    def create(self, request, *args, **kwargs):
        data=request.DATA.copy()
        data['report'] = kwargs['pk']
        files=request.FILES
        serializer = self.get_serializer(data=data, files=files)
        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=False)

            ri = self.object
            
            # if this image is the report thumb, pass it to the report
            # save() will also make resized and cropped version of the thumb
            if ('is_thumb' in data and data['is_thumb'] == 'true'):
                ri.report.image = self.object.image
                ri.report.image_crop_x = float(data['thumb_crop_x'])
                ri.report.image_crop_y = float(data['thumb_crop_y'])
                ri.report.image_crop_w = float(data['thumb_crop_w'])
                ri.report.image_crop_h = float(data['thumb_crop_h'])
                ri.report.save()
            
            data = {
                'report_id' : ri.report_id,
                'team' : ri.team,
                'position': ri.position,
                'message': 'Report image created.'
            }
            return Response(data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReportImageDetail(generics.RetrieveUpdateDestroyAPIView):
    model = ReportImage
    serializer_class = ReportImageSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, CountryEnabledOrReadOnly)


    def get_object(self):
        qs = self.model._default_manager.all()
        report_id = self.kwargs.get('report_id', None)
        team = self.kwargs.get('team', NOTEAM)
        position = self.kwargs.get('position', None)
        if report_id is None or position is None:
            raise AttributeError("Missing arguments pk ({0}) or position ({1})".format(report_id, position))
        try:
            obj = qs.get(report_id=report_id, team=team, position=position)
        except ObjectDoesNotExist:
            raise Http404('Report image with report_id={0}, team={1}, position={2} does not exists.'.format(report_id, team, position))
        return obj

    def pre_save(self, obj):
        super(ReportImageDetail, self).pre_save(obj)


    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        self.object = self.get_object()
        data=request.DATA
        files=request.FILES
        serializer = self.get_serializer(self.object, data=data, files=files, partial=partial)
        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_update= True)
            self.post_save(self.object, created=False)
            ri = self.object
            data = {
                'report_id' : ri.report_id,
                'position': ri.position,
                'message': 'Report image updated.'
            }
            return Response(data, status=status.HTTP_200_OK)


        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




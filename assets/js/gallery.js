


/* Initial values for global variables. */
 
// ISO2 of the country we're in
var cLocalCountryIso2 = '';

// Item types
var cTypeImage = 0;
var cTypeParagraph = 1;
var cTypeVideo = 2;

// available sizes of pictures
var gSourceSizes = [];

// image & paragraph items
var gItems = [];

// currently selected size
var gSourceSizeIndex = 0;

// currently selected item
var gItemIndex = 0;

// previously selected item
var gPrevItemIndex = 0;

// display full description instead of just 1 line?
var gFullDescription = false;

// display description in local language?
var gLocalDescription = false;

// light or dark theme?
var gLightDisplay = false;

// current position of the thumbs slider
var gThumbsPosition = 0;

// interval id for movement animation
var gThumbsAnimId = 0;

// do we use preloader animation while switching images?
// currently only for Safari
var gPreloader = (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1);

  
function galleryInit(params) {
	
	/* Get stuff from params. */
	cLocalCountryIso2 = params.localCountryIso2;
  gSourceSizes = params.sourceSizes;
  gItems = params.items;
  

  /* Try to parse initial values from the hash part of URL. */
  var itemIndex = parseInt(location.hash.substring(1)) - 1;
  if (!isNaN(itemIndex)) {
    gPrevItemIndex = gItemIndex = itemIndex;
  }	
  
  
  /* Loads settings from cookies. */
	gLightDisplay = (cookieGet('lightDisplay') == 'true');
  

	/* Hook up event for init after the page loads. */  
  $(document).ready(function () {
  	onResized();
    onImageChanged();
    onLightsChanged();
    $(window).resize(function() {
        onResized();
    });
    document.onkeydown = onKeyDown;
  });

  
  /* In case we returned back in history. */
  showExitMessage(false);
}


/* When description should update. */
function onDescriptionChanged() {
	
  if (gLocalDescription) {
    $('#gallery-description-control-language').html('[EN]');
    $('#gallery-description').html(gItems[gItemIndex].localDesc);
    if (!$('#gallery-description').html()) {
      $('#gallery-description').html('<p>/translation into local language not available/</p>');
    }
  } else {
    $('#gallery-description-control-language').html('['+cLocalCountryIso2+']');
    $('#gallery-description').html(gItems[gItemIndex].desc);
  }
  
  if (!gItems[gItemIndex].localDesc && !gItems[gItemIndex].desc) {
    $('#gallery-description-controls').hide();
  } else {
    $('#gallery-description-controls').show();
  }

  // prevent it from shrinking    
  if (!$('#gallery-description').html()) {
    $('#gallery-description').html('<p>&nbsp;</p>');
  }
  
  if (gFullDescription) {
    $('#gallery-description').removeClass('gallery-description-brief');
    $('#gallery-description-control-moreless-more').hide();
    $('#gallery-description-control-moreless-less').show();
  } else {
    $('#gallery-description').addClass('gallery-description-brief');
    $('#gallery-description-control-moreless-more').show();
    $('#gallery-description-control-moreless-less').hide();
  }
} 


/* After the current item is changed. Loads new image and updates elements. */
function onImageChanged() {
  
  // change selected thumb
  $('#gallery-thumb' + gPrevItemIndex).removeClass('gallery-thumb-selected');
  $('#gallery-thumb' + gItemIndex).addClass('gallery-thumb-selected');
  gPrevItemIndex = gItemIndex;
  thumbsMakeItemVisible(gItemIndex);
  
  // detect source size to use based on window size
  gSourceSizeIndex = gSourceSizes.length - 1;
  for (var i=0; i<gSourceSizes.length; i++) {
    if ($(window).width() <= gSourceSizes[i] || $(window).height() <= gSourceSizes[i]) {
      gSourceSizeIndex = i;
      break;
    }
  }

  
  if (gItems[gItemIndex].type == cTypeImage) {
    
    $('#gallery-description-container').show();

    onDescriptionChanged();
    
    // download link
    $('#gallery-download').attr('href', gItems[gItemIndex].imageOriginal);

    // load image
    $('#gallery-image').attr('src', "");
  	$('#gallery-image').attr('src', gItems[gItemIndex].images[gSourceSizeIndex]);
    $('#gallery-paragraph-container').hide();
    $('#gallery-video-container').hide();

    if (gPreloader) {
	    $('#gallery-description-container').hide();
	    $('#gallery-image').hide();
    	$('#gallery-preloader-container').show();
    	$('#gallery-image').load(function(){
		    $('#gallery-description-container').show();
		    $('#gallery-image').show();
	    	$('#gallery-preloader-container').hide();
	    	onResized();
    	});
	  } else {
	    $('#gallery-image').show();
	  	$('#gallery-image').load(null); // remove previous callback
	    onResized();
	  }    

  } else if (gItems[gItemIndex].type == cTypeVideo) {

    $('#gallery-description-container').show();

    onDescriptionChanged();

    $('#gallery-image').hide();
    $('#gallery-paragraph-container').hide();
    $('#gallery-video-container').show();
    
    $('#gallery-video-container').html(gItems[gItemIndex].videoHTML);
    
    // disable download link
    $('#gallery-download').removeAttr('href');
    
    onResized();
  } else if (gItems[gItemIndex].type == cTypeParagraph) {

    $('#gallery-image').hide();
    $('#gallery-paragraph-container').show();
    $('#gallery-video-container').hide();
    
    $('#gallery-paragraph-english').html(gItems[gItemIndex].desc);
    $('#gallery-paragraph-local').html(gItems[gItemIndex].localDesc);
    if (gItems[gItemIndex].localDesc) {
      $('#gallery-paragraph-local').show();
    } else {
      $('#gallery-paragraph-local').hide();
    }

    $('#gallery-description-container').hide();
    
    // disable download link
    $('#gallery-download').removeAttr('href');
    
    onResized();
  }
  
  
  // nav buttons
  if (gItemIndex > 0) {
    $('#gallery-prev').show();
  } else {
    $('#gallery-prev').hide();
  }
  if (gItemIndex < gItems.length-1) {
    $('#gallery-next').show();
  } else {
    $('#gallery-next').hide();
  }
  
  // url change to reflect curent image
  location.replace('#' + (gItemIndex+1));
}


/* After the image or canvas is resized. Layouts the elements. */
function onResized() {
  var jContainer = $('#gallery-item-container');
  var jSubcontainer = $('#gallery-item-subcontainer');
  var jImg = $('#gallery-image');
  var jParagraph = $('#gallery-paragraph-container');
  var jVideo = $('#gallery-video-container');
  var jPreloaderContainer = $('#gallery-preloader-container');
  var jMenubar = $('#gallery-menubar');
  var jDescriptionContainer = $('#gallery-description-container');
  var jDescriptionSubcontainer = $('#gallery-description-subcontainer');
  var jDescriptionText = $('#gallery-description');
  var jDescriptionControls = $('#gallery-description-controls');
  var jThumbsContainer = $('#gallery-thumbs-container');
  
  // reset
  $(jContainer).height("");
  $(jDescriptionContainer).height("");
  $(jDescriptionText).width("");
  $(jDescriptionSubcontainer).width("");
  

  // adjust description box
  if ($(window).width() < 600) {
    $(jDescriptionContainer).css({'padding-left': '10px', 'padding-right': '10px'});
    $(jDescriptionSubcontainer).width($(window).width() - 20);
    $(jDescriptionText).css({'text-align': 'left'});
  } else {
    $(jDescriptionContainer).css({'padding-left': '40px', 'padding-right': '40px'});
    $(jDescriptionSubcontainer).width($(window).width() - 80);
    $(jDescriptionText).css({'text-align': 'justify'});
  }
  $(jDescriptionText).width($(jDescriptionSubcontainer).width() - $(jDescriptionControls).width());
  $(jDescriptionContainer).height($(jDescriptionContainer).height());


  // determine max bounds for the item    
  var maxWidth = $(window).width();
  var maxHeight = $(window).height() - $(jMenubar).outerHeight();
  if (gItems[gItemIndex].type == cTypeImage || gItems[gItemIndex].type == cTypeVideo) {
  	maxHeight -= $(jDescriptionContainer).height();
  } else {
  	maxHeight += 5;
  }
  if ($(window).height() >= 600) {
    maxHeight -= $(jThumbsContainer).height() + 20;
    $('#gallery').removeClass('gallery-scroll');
  } else {
    $('#gallery').addClass('gallery-scroll');
	}  	

	// fix the container size
  $(jContainer).height(maxHeight);

	// fix preloader
	$(jPreloaderContainer).height(maxHeight + 30);
	

  if (gItems[gItemIndex].type == cTypeImage) {
    
    // find out how much space we have left for the image
	  maxWidth -= parseInt($(jSubcontainer).css('padding-left')) + parseInt($(jSubcontainer).css('padding-right'));
	  maxHeight -= parseInt($(jSubcontainer).css('padding-top')) + parseInt($(jSubcontainer).css('padding-bottom'));
    maxWidth = Math.min(gSourceSizes[gSourceSizeIndex], maxWidth);
    maxHeight = Math.min(gSourceSizes[gSourceSizeIndex], maxHeight);

    // determine new image bounds
    var w = 0;
    var h = 0;
    var aspectRatio = gItems[gItemIndex].imageAspectRatio;
    
    if (aspectRatio == 0) { // invalid image?
      w = maxWidth;
      h = maxHeight;
    } else {
      // proportional scaling to fit into defined max bounds
      if (aspectRatio > maxWidth / maxHeight ) {
      	w = maxWidth;
      	h = maxWidth / aspectRatio;
      } else {
      	w = maxHeight * aspectRatio;
      	h = maxHeight;
      }      
    }

    // layout the image
    $(jImg).width(w);
    $(jImg).height(h);
  } else if (gItems[gItemIndex].type == cTypeVideo) {
	  maxWidth -= parseInt($(jSubcontainer).css('padding-left')) + parseInt($(jSubcontainer).css('padding-right'));
	  maxHeight -= parseInt($(jSubcontainer).css('padding-top')) + parseInt($(jSubcontainer).css('padding-bottom'));

    // layout the video
    $(jVideo).width(maxWidth);
    $(jVideo).height(maxHeight);
  } else if (gItems[gItemIndex].type == cTypeParagraph) {
  	maxHeight -= 30; // paddings
		$(jParagraph).height(maxHeight);
  }
}


/* When we switched dark/light mode */
function onLightsChanged() {
  if (gLightDisplay) {
    $('#gallery').removeClass('gallery--dark');
    $('.gallery-menu-item').removeClass('gallery-menu-item--dark');
    $('.gallery-paragraph').removeClass('gallery-paragraph--dark');
    $('.gallery-description').removeClass('gallery-description--dark');
    $('.gallery-description-control-text').removeClass('gallery-description-control-text--dark');
    $('#gallery-thumbs-container').removeClass('gallery-thumbs-container--dark');
    $('.gallery-thumb').removeClass('gallery-thumb--dark');
  	$('#gallery-preloader').attr('src', "/static/images/preloader-black.gif");
  } else {
    $('#gallery').addClass('gallery--dark');
    $('.gallery-menu-item').addClass('gallery-menu-item--dark');
    $('.gallery-paragraph').addClass('gallery-paragraph--dark');
    $('.gallery-description').addClass('gallery-description--dark');
    $('.gallery-description-control-text').addClass('gallery-description-control-text--dark');
    $('#gallery-thumbs-container').addClass('gallery-thumbs-container--dark');
    $('.gallery-thumb').addClass('gallery-thumb--dark');
  	$('#gallery-preloader').attr('src', "/static/images/preloader-white.gif");
  }
}


function onKeyDown(e) {
  e = e || window.event;
  if (e.keyCode == 37) {
    prevImage();
  } else if (e.keyCode == 39) {
    nextImage();
  }
}

function prevImage() {
  if (gItemIndex > 0) {
    gItemIndex--;
    onImageChanged();
  }    
}

function nextImage() {
  if (gItemIndex < gItems.length-1) {
    gItemIndex++;
    onImageChanged();
  }    
}

function hoverPrevNext(event) {
	$(event).addClass('gallery-prevnext-hover');
}

function unhoverPrevNext(event, delayMillis) {
	setTimeout(function(){
		$(event).removeClass('gallery-prevnext-hover');
	}, delayMillis);
}

function selectImage(index) {
  gItemIndex = index;
  onImageChanged();
}

function toggleFullDescription() {
  gFullDescription = !gFullDescription;
  onDescriptionChanged();
  onResized();
}

function toggleLocalDescription() {
  gLocalDescription = !gLocalDescription;
  onDescriptionChanged();
  onResized();
}

function toggleLights() {
  gLightDisplay = !gLightDisplay;
  cookieSet('lightDisplay', gLightDisplay, 30);
  console.log(document.cookie);
  onLightsChanged();
}

function thumbsMoveToPosition(targetPosition) {
  var delay = 1000/60;
  var movementSpeed = delay * 2;
  
  // cancel current movement if there is any
  if (gThumbsAnimId) {
    clearInterval(gThumbsAnimId);
  }
  
  gThumbsAnimId = setInterval(function frame() {
    
    if (targetPosition > gThumbsPosition) {
      gThumbsPosition = Math.min(targetPosition, gThumbsPosition + movementSpeed);
    } else if (targetPosition < gThumbsPosition) {
      gThumbsPosition = Math.max(targetPosition, gThumbsPosition - movementSpeed);
    }      

    $('#gallery-thumbs').css('left', -gThumbsPosition + 'px'); 
    
    if (gThumbsPosition == targetPosition) {
      clearInterval(gThumbsAnimId);
    }
    
  }, delay);
}

function thumbsMakeItemVisible(index) {
  var itemSize = $('.gallery-thumb:first').outerWidth();
  var visibleSize = $('#gallery-thumbs-content').width();
  var itemPosition = $('.gallery-thumb').eq(index).position().left;
  if (itemPosition < gThumbsPosition) {
    thumbsMoveToPosition(itemPosition);
  } else if (itemPosition > gThumbsPosition + visibleSize - itemSize) {
    thumbsMoveToPosition(itemPosition);
  }
}

function thumbsScroll(direction) {
  var visibleSize = $('#gallery-thumbs-content').width();
  var contentSize = $('#gallery-thumbs').width();
  var targetPosition = gThumbsPosition + direction * visibleSize;
  targetPosition = Math.max(0, Math.min(contentSize - visibleSize, targetPosition));
  thumbsMoveToPosition(targetPosition);
}

function showExitMessage(show) {
	if (show) {
		$('#gallery-fullscreen-message').show();
	} else {
		$('#gallery-fullscreen-message').hide();
  }
}


function delayedRedirect(url, delayMillis) {
	setTimeout(function(){
		window.location.href=url;
	}, delayMillis);
}


function cookieSet(name, value, expires, path, domain) {
  var cookie = name + "=" + escape(value) + ";";
 
  if (expires) {
    // If it's a date
    if(expires instanceof Date) {
      // If it isn't a valid date
      if (isNaN(expires.getTime()))
       expires = new Date();
    } else {
      expires = new Date(new Date().getTime() + parseInt(expires) * 1000 * 60 * 60 * 24);
    }
 
    cookie += "expires=" + expires.toGMTString() + ";";
  }
 
  if (path) {
    cookie += "path=" + path + ";";
  }
  if (domain) {
    cookie += "domain=" + domain + ";";
  }
 
  document.cookie = cookie;
}


function cookieGet(name) {
  var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
  var result = regexp.exec(document.cookie);
  return (result === null) ? null : result[1];
}

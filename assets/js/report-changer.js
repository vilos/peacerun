

var gReports = [];

var gIndex = 0;

var gAutoChangeTimer = 0;


function reportChangerInit(params)
{
	gReports = params.reports;
}


function reportChangerChangeTo(index)
{
  $("#report_link").attr("href", gReports[index].url);
  $("#report_title").attr("href", gReports[index].url);
  $("#report_title").text(gReports[index].title);
  var headline = gReports[index].headline;
  if (gReports[index].headlineLocal != "") {
  	headline += "<br>";
  	headline += gReports[index].headlineLocal;
  }
  $("#report_headline").html(headline);
  
  $('#report_image').fadeOut(100, function() {
    $(this).load(function() { $(this).fadeIn(100); });
    $(this).attr("src", gReports[index].src);
  });
  
  gIndex = index;
}


function reportChangerStartAuto()
{
	reportChangerStopAuto();
	gAutoChangeTimer = setInterval("reportChangerRandom()", 5000);
}


function reportChangerStopAuto()
{
	if (gAutoChangeTimer != 0) {
		clearInterval(gAutoChangeTimer);
		gAutoChangeTimer = 0;
	}
}


function reportChangerPrev() 
{
	var index = gIndex - 1;
	if (index < 0) index = gReports.length - 1;
	reportChangerChangeTo(index);
}


function reportChangerNext() 
{
	var index = gIndex + 1;
	if (index >= gReports.length) index = 0;
	reportChangerChangeTo(index);
}


function reportChangerRandom()
{
	if (gReports.length <= 1) return;
	
	var newindex = gIndex;
	while (newindex == gIndex) {
		newindex = Math.floor(Math.random() * (gReports.length));
	};
	reportChangerChangeTo(newindex);
}